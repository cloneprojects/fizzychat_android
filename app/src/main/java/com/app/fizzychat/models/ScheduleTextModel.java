package com.app.fizzychat.models;

public class ScheduleTextModel {
    public String getUser_id() {
        return user_id;
    }

    public String getGroup_id() {
        return group_id;
    }


    public String getAdded_by() {
        return added_by;
    }

    public String getMessage() {
        return message;
    }

    public String getStatus() {
        return status;
    }

    String user_id;
    String group_id;

    public long getDeliverTime() {
        return deliverTime;
    }

    long deliverTime;
    String added_by;
    String message;
    String status;

    public String getUserName() {
        return userName;
    }

    String userName;

    public ScheduleTextModel(String user_id, String group_id, long deliverTime, String added_by, String message, String status, String userName) {
        this.user_id = user_id;
        this.group_id = group_id;
        this.deliverTime = deliverTime;
        this.added_by = added_by;
        this.message = message;
        this.status = status;
        this.userName = userName;
    }


} 