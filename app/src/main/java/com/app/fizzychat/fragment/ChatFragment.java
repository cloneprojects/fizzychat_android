package com.app.fizzychat.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.Html;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;
import com.app.fizzychat.DBHelper.DBHandler;
import com.app.fizzychat.R;
import com.app.fizzychat.activity.MainActivity;
import com.app.fizzychat.activity.NewBroadCast;
import com.app.fizzychat.activity.NewGroup_activity;
import com.app.fizzychat.activity.QRCodeActivity;
import com.app.fizzychat.activity.ScheduleText;
import com.app.fizzychat.activity.SettingsActivity;
import com.app.fizzychat.activity.Splash_activity;
import com.app.fizzychat.activity.StarredMessages;
import com.app.fizzychat.adapter.ChatsAdapter;
import com.app.fizzychat.baseUtils.AppController;
import com.app.fizzychat.baseUtils.AsyncTaskCompleteListener;
import com.app.fizzychat.baseUtils.Const;
import com.app.fizzychat.baseUtils.PostHelper;
import com.app.fizzychat.baseUtils.SharedHelper;
import com.app.fizzychat.baseUtils.Utils;
import com.app.fizzychat.models.GetAllChats;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static android.app.Activity.RESULT_CANCELED;

/**
 * Created by KrishnaDev on 12/30/16.
 */
public class ChatFragment extends Fragment implements SearchView.OnQueryTextListener {
    public static ChatFragment mainActivity = null;
    public static Boolean channel_enabled = false;
    //action mode
    public static int counter = 0;
    public static ChatsAdapter chatsAdapter;
    public static String isGroup = "true";
    public static String isSchedule = "false";
    public static RecyclerView recyclerView;
    public static RelativeLayout errorLayout;
    public static List<JSONObject> chatsList;
    public static DBHandler dbHandler;
    public static List<JSONObject> pinnedchatsList = new ArrayList<>();
    public static List<JSONObject> normalchatsList = new ArrayList<>();
    View viewSeperator;
    FloatingActionMenu actionButton;
    boolean isLoaded = false;
    private List<FloatingActionMenu> menus = new ArrayList<>();
    private FloatingActionButton new_group;
    private FloatingActionButton new_chat;
    private boolean internet;
    private MainActivity main_activity;
    private List<JSONObject> s_chatslist = new ArrayList<>();
    private Handler handler = new Handler();
    private int myCount = 6;
    private String TAG = ChatFragment.class.getSimpleName();

    public ChatFragment() {
        mainActivity = this;
        setHasOptionsMenu(true);
    }

    public static void refreshLay() {
        try {
            mainActivity.getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    GetAllChats getAllChats = dbHandler.GetCompleteChats();
                    normalchatsList = getAllChats.getNormalChatList();
                    pinnedchatsList = getAllChats.getPinnedChatList();


                    List<JSONObject> alljsonObjects = new ArrayList<>();
                    alljsonObjects.addAll(pinnedchatsList);
                    alljsonObjects.addAll(normalchatsList);
                    chatsAdapter.changeValues(alljsonObjects);
//                    chatsAdapter = new ChatsAdapter(mainActivity.getActivity(), alljsonObjects);
//                    recyclerView.setAdapter(chatsAdapter);
                    chatsAdapter.notifyDataSetChanged();


                }
            });
        } catch (Exception e) {
        }
    }

    public static int getPrimaryCOlor(Context context) {
        final TypedValue value = new TypedValue();
        context.getTheme().resolveAttribute(R.attr.colorPrimary, value, true);
        return value.data;
    }

    public static int getPrimaryDark(Context context) {
        final TypedValue value = new TypedValue();
        context.getTheme().resolveAttribute(R.attr.colorPrimaryDark, value, true);
        return value.data;
    }

    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public void changecolour(MenuItem menuItem) {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);

        View view = inflater.inflate(R.layout.fragment_chat, container, false);
        Log.d("onCreateView: ", "came_home");
        errorLayout = (RelativeLayout) view.findViewById(R.id.error_layout);
        errorLayout.setVisibility(View.GONE);
        dbHandler = new DBHandler(getActivity());
        recyclerView = (RecyclerView) view.findViewById(R.id.chats_list);

        try {
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
            recyclerView.addItemDecoration(new HorizontalDividerItemDecoration.Builder(getActivity())
                    .color(Color.parseColor("#dfdfdf"))
                    .sizeResId(R.dimen.divider)
                    .marginResId(R.dimen.leftmargin, R.dimen.rightmargin)
                    .build());
            recyclerView.setLayoutManager(linearLayoutManager);

//////////pinned chats
            LinearLayoutManager linearLayoutManager1 = new LinearLayoutManager(getActivity());
            dbHandler.close();


            recyclerView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    Log.d(TAG, "onGlobalLayout: Loaded");
//                    loadRemainingDatas();
                    if (dbHandler.getChatRoomCount() > 10) {
                        loadRemainginDatas();
                    }

                    recyclerView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                }
            });

            loadFirstTime();


        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }

    private void loadRemainginDatas() {


        GetAllChats getAllChats = dbHandler.GetRemainingChatRoom();
        normalchatsList = getAllChats.getNormalChatList();

        pinnedchatsList = getAllChats.getPinnedChatList();
//

        List<JSONObject> alljsonObjects = new ArrayList<>();
        alljsonObjects.addAll(pinnedchatsList);
        alljsonObjects.addAll(normalchatsList);
        chatsAdapter.addDatas(alljsonObjects);


    }


    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onStop() {
        super.onStop();

    }

    @Override
    public void onResume() {
        super.onResume();
//        resetMethod();
        Log.d(TAG, "onResume: Chat");


    }

    public void resetSelection() {
        try {
            chatsAdapter.updateAllKeyAndValue("isSelected", "false");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


//    public void refresh() {
//
//        try {
//
//            runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//                    dbHandler = new DBHandler(main_activity);
//                    GetAllChats getAllChats = dbHandler.GetCompleteChats();
//                    normalchatsList = getAllChats.getNormalChatList();
//                    pinnedchatsList = getAllChats.getPinnedChatList();
//
//                    List<JSONObject> alljsonObjects = new ArrayList<>();
//                    alljsonObjects.addAll(pinnedchatsList);
//                    alljsonObjects.addAll(normalchatsList);
//
//
////                    if (pinnedchatsList.size() > 0) {
////                        pinned_chats_list.setVisibility(View.VISIBLE);
////                        pinnedChatsAdapter = new PinnedChatsAdapter(getActivity(), pinnedchatsList);
////                        pinned_chats_list.setAdapter(pinnedChatsAdapter);
////                    }
//                    if (normalchatsList.size() > 0) {
////                        chatsAdapter = new ChatsAdapter(getActivity(), alljsonObjects);
////                        recyclerView.setAdapter(chatsAdapter);
////                        errorLayout.setVisibility(View.GONE);
//                        chatsAdapter.changeValues(alljsonObjects);
//                    } else {
//                        if (normalchatsList.size() == 0 && pinnedchatsList.size() == 0) {
//                            errorLayout.setVisibility(View.VISIBLE);
//                        }
//                    }
//                    MainActivity.unreadCount++;
//
//                }
//            });
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        main_activity = (MainActivity) activity;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        Log.d(TAG, "onCreateOptionsMenu:Chat ");

        MainActivity.mainActivity.toolbar.getMenu().clear();
        MainActivity.mainActivity.toolbar.setNavigationIcon(null);
        MainActivity.mainActivity.counterTextView.setText(R.string.app_name);
        inflater.inflate(R.menu.chat_main, menu);
        if (ChatsAdapter.is_in_action_mode) {
            resetToolbarAndCount();
            try {
                chatsAdapter.updateAllKeyAndValue("isSelected", "false");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        for (int i = 0; i < menu.size(); i++) {
            MenuItem menuItem = menu.getItem(i);
            Drawable drawable = menuItem.getIcon();
            if (drawable != null) {
                // If we don't mutate the drawable, then all drawable's with this id will have a color
                // filter applied to it.
                drawable.mutate();
                drawable.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);

            }

        }
        MenuItem menuItem = menu.findItem(R.id.contact_search);
//        Refreshfrommain();
        try {
            SearchView view = (SearchView) MenuItemCompat.getActionView(menuItem);
            EditText searchEditText = (EditText) view.findViewById(android.support.v7.appcompat.R.id.search_src_text);
            searchEditText.setHintTextColor(getActivity().getResources().getColor(R.color.white));

            view.setOnQueryTextListener(this);
            MenuItemCompat.setOnActionExpandListener(menuItem, new MenuItemCompat.OnActionExpandListener() {
                @Override
                public boolean onMenuItemActionExpand(MenuItem item) {
//                if (chatsList.size() > 0)
//
//                    chatsAdapter.setFilter(chatsList);

                    return true;
                }

                @Override
                public boolean onMenuItemActionCollapse(MenuItem item) {
                    return true;
                }
            });

        } catch (Exception e) {

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.settings:
                Intent setti_full = new Intent(getActivity(), SettingsActivity.class);
                setti_full.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(setti_full);
                break;

            case R.id.new_channel:
                Intent channel = new Intent(getActivity(), NewGroup_activity.class);
                isGroup = "Channel";
                channel.putExtra("forward_msg", "false");

                channel.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(channel);
                break;

            case R.id.web:
                Intent web = new Intent(getActivity(), QRCodeActivity.class);
                web.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(web);
                break;


            case R.id.schedule_text:
                Intent schedulle = new Intent(getActivity(), ScheduleText.class);
                schedulle.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivityForResult(schedulle, 1);

                break;


            case R.id.starred_msgs:
                Intent star = new Intent(getActivity(), StarredMessages.class);
                star.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(star);
                break;


            case R.id.action_lock:
                Utils.showSimpleProgressDialog(getActivity(), "Please wait...", false);
                checkstatus();
                break;


            case R.id.action_un_lock:
                showalertpassunlock();
                break;

            case R.id.action_pin_chat:
                // Utils.showSimpleProgressDialog(getActivity(), "Please wait...", false);

                Log.e("size", "totalSize:" + AppController.numOfItemsSelected);
                Log.e("size", "selectRemoveItem:" + ChatsAdapter.selectRemoveItem);
                // Log.e("size", "total_already_pinned:" + getPinnedChats(chatsList).size());
                int x = ChatsAdapter.selectRemoveItem.size() + (pinnedchatsList.size());


                if (x >= 4) {
                    Utils.showShortToast("You can only pin up to 3 chats ", getActivity());
                } else {
//                    chatsList = dbHandler.GetChats();
                    int count = dbHandler.getPinnedCount();

                    if (count < 3) {

                        pinChat();

                    } else {
                        Utils.showShortToast("You can only pin up to 3 chats ", getActivity());
                    }
                }
                break;


            case R.id.action_unpin_chat:
                // Utils.showSimpleProgressDialog(getActivity(), "Please wait...", false);
                unPinChat();


                break;


            case R.id.action_shortcut:

                String userName = "", userImage = "", chatRoomType = "", createBy = "", groupImage = "", ChatRoomId = "";
                try {
                    // userName = dbHandler.GetUserName(ChatsAdapter.selectRemoveItem.get(0));

                    chatRoomType = dbHandler.getChatRoomTypeFromChats(ChatsAdapter.selectRemoveItem.get(0));


                    ChatRoomId = (ChatsAdapter.selectRemoveItem.get(0));

                    if (chatRoomType.equalsIgnoreCase("0")) {
                        userName = dbHandler.GetUserName(ChatsAdapter.selectRemoveItem.get(0));
                        userImage = dbHandler.GetUserImage(ChatsAdapter.selectRemoveItem.get(0));
                    } else {
                        userName = dbHandler.GetGroupName(ChatsAdapter.selectRemoveItem.get(0));
                        createBy = dbHandler.getCreateby(ChatsAdapter.selectRemoveItem.get(0));
                        groupImage = dbHandler.GetGroupImage(ChatsAdapter.selectRemoveItem.get(0));
                    }
                    Log.e("unPinShort", "action_shortcut:" + userName + "$$$$" + userImage + "$$$$" + chatRoomType + "$$$$" + ChatRoomId + "$$$" + createBy + "&&&&" + groupImage);


                    Log.e("unPinShort", "userName:" + userName + "$$$$");
                    Log.e("unPinShort", "userImage:" + userImage + "$$$$");
                    Log.e("unPinShort", "chatRoomType:" + chatRoomType + "$$$$");
                    Log.e("unPinShort", "createBy:" + createBy + "$$$$");
                    Log.e("unPinShort", "groupImage:" + groupImage + "$$$$");
                    Log.e("unPinShort", "ChatRoomId:" + ChatRoomId + "$$$$");

                } catch (Exception e) {

                }


                if (AppController.whichRecylerSelected == 1)
                    createShortCut(userName, userImage, chatRoomType, ChatRoomId, recyclerView.getChildAt(ChatsAdapter.chatShortCutPos), createBy, groupImage);

                Utils.showShortToast("Chat shortcut added to your home screen", getActivity());

                resetToolbarAndCount();


                break;


            case R.id.new_group:
                Intent group = new Intent(getActivity(), NewGroup_activity.class);
                isGroup = "false";
                group.putExtra("forward_msg", "false");
                group.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivityForResult(group, 1);
                break;

            case R.id.new_broadcast:
                Intent broad = new Intent(getActivity(), NewBroadCast.class);
                isGroup = "false";
                broad.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(broad);
                break;

            case R.id.action_delete:
                Log.e("Hello", "World");

                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.AlertDialogCustom);

                builder.setTitle("");

                builder.setMessage("Are you sure you want to delete chats?");

                builder.setPositiveButton(Html.fromHtml("<font color='#00897b'>OK</font>"), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        deletechat();
                        dialog.dismiss();
                    }
                });
                builder.setNegativeButton(Html.fromHtml("<font color='#00897b'>Cancel</font>"), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                builder.show();
                break;
        }
        return true;
    }


    private void unPinChat() {

        new AsyncTask<String, String, String>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                Utils.showSimpleProgressDialog(getActivity(), "Please wait...", false);
            }

            @Override
            protected String doInBackground(String... strings) {
                DBHandler dbHandler = new DBHandler(getActivity());
                if (ChatsAdapter.selectRemoveItem.size() > 0) {
                    for (int i = 0; i < ChatsAdapter.selectRemoveItem.size(); i++) {

                        Log.e("chat", "unpinItem:" + ChatsAdapter.selectRemoveItem.get(i));

                        dbHandler.UnPinnedChatUpdate(ChatsAdapter.selectRemoveItem.get(i));
                    }
                }
                dbHandler.close();
                return null;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                Utils.removeProgressDialog();

                if (ChatsAdapter.selectRemoveItem.size() > 0) {
                    for (int i = 0; i < ChatsAdapter.selectRemoveItem.size(); i++) {
                        Log.e("chat", "selectRemoveItem:" + ChatsAdapter.selectRemoveItem.get(i));
                        try {
                            chatsAdapter.updateChatPinned(ChatsAdapter.selectRemoveItem.get(i), "false");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }


                Utils.showShortToast("Chat unpinned sucessfully", getActivity());
                resetToolbarAndCount();

            }
        }.execute();


    }


    private void pinChat() {
        new AsyncTask<String, String, String>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                Utils.showSimpleProgressDialog(getActivity(), "Please wait...", false);
            }

            @Override
            protected String doInBackground(String... strings) {
                DBHandler dbHandler = new DBHandler(getActivity());
                if (ChatsAdapter.selectRemoveItem.size() > 0) {
                    for (int i = 0; i < ChatsAdapter.selectRemoveItem.size(); i++) {

                        Log.e("chat", "selectRemoveItem:" + ChatsAdapter.selectRemoveItem.get(i));

                        dbHandler.PinnedChatUpdate(ChatsAdapter.selectRemoveItem.get(i));

                    }
                }
                dbHandler.close();
                return null;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                Utils.removeProgressDialog();
                if (ChatsAdapter.selectRemoveItem.size() > 0) {
                    for (int i = 0; i < ChatsAdapter.selectRemoveItem.size(); i++) {
                        Log.e("chat", "selectRemoveItem:" + ChatsAdapter.selectRemoveItem.get(i));
                        try {
                            chatsAdapter.updateChatPinned(ChatsAdapter.selectRemoveItem.get(i), "true");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }

                AppController.numOfItemsSelected = 0;
                resetToolbarAndCount();
                Utils.showShortToast("Chat pinned sucessfully", getActivity());
//                ChatFragment.mainActivity.resetMethod();

            }
        }.execute();


    }

    private void deletechat() {
        new AsyncTask<String, String, String>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                Utils.showSimpleProgressDialog(getActivity(), "Please wait...", false);
            }

            @Override
            protected String doInBackground(String... strings) {
                DBHandler dbHandler = new DBHandler(getActivity());
                if (ChatsAdapter.selectRemoveItem.size() > 0) {
                    for (int i = 0; i < ChatsAdapter.selectRemoveItem.size(); i++) {
                        dbHandler.GroupDeleteUpdate("1", ChatsAdapter.selectRemoveItem.get(i));
                        dbHandler.DeleteChats(ChatsAdapter.selectRemoveItem.get(i));
                        dbHandler.DeleteGrpMsg(ChatsAdapter.selectRemoveItem.get(i));
                        dbHandler.DeleteChatsComplete(ChatsAdapter.selectRemoveItem.get(i));
                        SharedHelper.putHeader(getActivity(), "header_time" + ChatsAdapter.selectRemoveItem.get(i), "0");
                        //dbHandler.UnPinChats(ChatsAdapter.selectRemoveItem.get(i));
                    }
                }

//                normalchatsList = dbHandler.GetChats();
                dbHandler.close();
                return null;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                Utils.removeProgressDialog();


                if (ChatsAdapter.selectRemoveItem.size() > 0) {
                    for (int i = 0; i < ChatsAdapter.selectRemoveItem.size(); i++) {
                        try {
                            chatsAdapter.deleteChatRoom(String.valueOf(ChatsAdapter.selectRemoveItem.get(i)));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        SharedHelper.putHeader(getActivity(), "header_time" + ChatsAdapter.selectRemoveItem.get(i), "0");
                        resetToolbarAndCount();
                        //dbHandler.UnPinChats(ChatsAdapter.selectRemoveItem.get(i));
                    }
                }

//                pinnedchatsList = dbHandler.GetPinnedChats();
//                if (normalchatsList.size() > 0) {
//                    chatsAdapter = new ChatsAdapter(getActivity(), normalchatsList);
//                    recyclerView.setAdapter(chatsAdapter);
//                }
//
//                Log.e("pinnedchatsList", pinnedchatsList.toString());
//
//                if (pinnedchatsList.size() > 0) {
//                    pinned_chats_list.setVisibility(View.VISIBLE);
//                    pinnedChatsAdapter = new PinnedChatsAdapter(getActivity(), pinnedchatsList);
//                    pinned_chats_list.setAdapter(pinnedChatsAdapter);
//                } else {
//                    if (normalchatsList.size() == 0 && pinnedchatsList.size() == 0) {
//                        errorLayout.setVisibility(View.VISIBLE);
//                    }
//
//                }
//                ChatFragment.mainActivity.clearActionM();
            }
        }.execute();

    }

    private void showalertpassunlock() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);

        dialog.setContentView(R.layout.password_layout_forgot);

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);


        final EditText password, confirm_password;
        Button submit;
        password = (EditText) dialog.findViewById(R.id.password);
        confirm_password = (EditText) dialog.findViewById(R.id.confirm_password);


        InputFilter filter = new InputFilter() {
            @Override
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                for (int i = start; i < end; i++)
                    if (Character.isSpaceChar(source.charAt(i)))
                        return "";

                return null;
            }
        };

        password.setFilters(new InputFilter[]{filter});
        confirm_password.setFilters(new InputFilter[]{filter});


        submit = (Button) dialog.findViewById(R.id.password_submit);

        TextView forgot_pass = (TextView) dialog.findViewById(R.id.pass_forgot);
        forgot_pass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new hitapiforotp("unlock").execute();
                Utils.showSimpleProgressDialog(main_activity, "Please Wait . . . .", false);
                dialog.dismiss();
            }
        });


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!password.getText().toString().equalsIgnoreCase("")) {

                    if (password.getText().toString().equalsIgnoreCase(confirm_password.getText().toString())) {
                        String confirm_password = dbHandler.GetPassword(ChatsAdapter.selectRemoveItem.get(0));

                        if (confirm_password.equalsIgnoreCase(password.getText().toString())) {
                            dbHandler.Updatelock(ChatsAdapter.selectRemoveItem.get(0), "0");
                            dbHandler.UpdatePassword(ChatsAdapter.selectRemoveItem.get(0), "");

                            try {
                                chatsAdapter.updateKeyAndValue(ChatsAdapter.selectRemoveItem.get(0), "Password", "");
                                chatsAdapter.updateKeyAndValue(ChatsAdapter.selectRemoveItem.get(0), "isLocked", "0");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }


//                            ChatFragment.refreshLay();
//                            refresh();
                            resetToolbarAndCount();
//                            clearActionM();
                            dialog.dismiss();
                        } else {
                            Toast.makeText(getActivity(), "Password Mismatch. Kindly try again", Toast.LENGTH_SHORT).show();

                        }


                    } else {
                        Toast.makeText(getActivity(), "Password Mismatch. Kindly try again", Toast.LENGTH_SHORT).show();
                    }


                } else {
                    Toast.makeText(getActivity(), "Password Should not be empty", Toast.LENGTH_SHORT).show();
                }
            }
        });

        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    private void checkstatus() {
        internet = Utils.isNetworkAvailable(getActivity());
        if (internet) {
            new checkmail().execute();
        } else {
            Toast.makeText(getContext(), "Check your connection", Toast.LENGTH_SHORT).show();
        }


    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
//        GetAllChats getAllChats = dbHandler.GetCompleteChats();
//        normalchatsList = getAllChats.getNormalChatList();
//
//        pinnedchatsList = getAllChats.getPinnedChatList();


        List<JSONObject> alljsonObjects = new ArrayList<>();
        alljsonObjects.addAll(pinnedchatsList);
        alljsonObjects.addAll(normalchatsList);

        try {

            if (newText.length() > 1) {
                s_chatslist = new ArrayList<>();
                Log.d("onQueryTextChange: ", "new_text_val:" + newText);
                s_chatslist = filter(alljsonObjects, newText);
//                Log.d("onQueryTextChange: ", "jsonobjects:" + getUserFromDBs);

//                for (int i = 0; i < getUserFromDBs.size(); i++) {
//                    Log.d("onQueryTextChange: ", "queriesvalue:" + getUserFromDBs.get(i));
//                    JSONObject object = getUserFromDBs.get(i);
//                    s_chatslist.add(object);
//                }

//                if (s_chatslist.size() > 0) {
//                    recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
//                    chatsAdapter = new ChatsAdapter(getActivity(), s_chatslist);
//                    recyclerView.setAdapter(chatsAdapter);
////                chatsAdapter.setFilter(getUserFromDBs);
//                    chatsAdapter.notifyDataSetChanged();
//                } else {
//                    s_chatslist = new ArrayList<>();
//                    recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
//                    chatsAdapter = new ChatsAdapter(getActivity(), s_chatslist);
//                    recyclerView.setAdapter(chatsAdapter);
////                chatsAdapter.setFilter(getUserFromDBs);
//                    chatsAdapter.notifyDataSetChanged();
//                }
//
                chatsAdapter.changeValues(s_chatslist);
            } else {


//                chatsAdapter = new ChatsAdapter(getActivity(), alljsonObjects);
//                recyclerView.setAdapter(chatsAdapter);
////                chatsAdapter.setFilter(getUserFromDBs);
//                chatsAdapter.notifyDataSetChanged();

                chatsAdapter.changeValues(alljsonObjects);

            }
        } catch (Exception e) {

        }
        return true;
    }

    private List<JSONObject> filter(List<JSONObject> chatsList, String newText) {
        newText = newText.toLowerCase();
        List<JSONObject> filter_list = new ArrayList<>();
        for (JSONObject getUserFromDB : chatsList) {
            String type_value = getUserFromDB.optString("Name").toLowerCase();
            Log.d("filter: ", "values:" + type_value + "," + newText);
            if (type_value.contains(newText)) {
                Log.d("filter: ", "addedvalues:" + getUserFromDB);
                filter_list.add(getUserFromDB);
            }
        }
        return filter_list;
    }


    public void resetToolbarAndCount() {

        ChatsAdapter.is_in_action_mode = false;
        AppController.isPinnedInActionMode = false;
        AppController.isNormalChatInActionMode = false;
        AppController.whichRecylerSelected = 0;
        MainActivity.mainActivity.toolbar.getMenu().clear();
        MainActivity.mainActivity.toolbar.inflateMenu(R.menu.chat_main);
        MainActivity.mainActivity.toolbar.setNavigationIcon(null);
        MainActivity.mainActivity.counterTextView.setText(R.string.app_name);

        Menu menu = MainActivity.mainActivity.toolbar.getMenu();
        MenuItem menuItem = menu.findItem(R.id.contact_search);
        SearchView view = (SearchView) MenuItemCompat.getActionView(menuItem);

        EditText searchEditText = (EditText) view.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        searchEditText.setHintTextColor(getActivity().getResources().getColor(R.color.white));

        view.setOnQueryTextListener(this);

        ChatsAdapter.selectRemoveItem.clear();
        try {
            ChatsAdapter.clearall();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        chatsAdapter.notifyDataSetChanged();
        counter = 0;

    }

    public void loadFirstTime() {

        try {
            ChatsAdapter.is_in_action_mode = false;
            AppController.isPinnedInActionMode = false;
            AppController.isNormalChatInActionMode = false;
            AppController.whichRecylerSelected = 0;
            MainActivity.mainActivity.toolbar.getMenu().clear();
            MainActivity.mainActivity.toolbar.inflateMenu(R.menu.chat_main);
            MainActivity.mainActivity.toolbar.setNavigationIcon(null);
            MainActivity.mainActivity.counterTextView.setText(R.string.app_name);

            Menu menu = MainActivity.mainActivity.toolbar.getMenu();
            MenuItem menuItem = menu.findItem(R.id.contact_search);
            SearchView view = (SearchView) MenuItemCompat.getActionView(menuItem);

            EditText searchEditText = (EditText) view.findViewById(android.support.v7.appcompat.R.id.search_src_text);
            searchEditText.setHintTextColor(getActivity().getResources().getColor(R.color.white));

            view.setOnQueryTextListener(this);

            GetAllChats getAllChats = dbHandler.GetCompleteChats();
            normalchatsList = getAllChats.getNormalChatList();

            pinnedchatsList = getAllChats.getPinnedChatList();
//

            List<JSONObject> alljsonObjects = new ArrayList<>();
            alljsonObjects.addAll(pinnedchatsList);
            alljsonObjects.addAll(normalchatsList);

//            if (pinnedchatsList.size() > 0) {
//                pinned_chats_list.setVisibility(View.VISIBLE);
//                pinnedChatsAdapter = new PinnedChatsAdapter(getActivity(), pinnedchatsList);
//                pinned_chats_list.setAdapter(pinnedChatsAdapter);
//            } else {
//                pinned_chats_list.setVisibility(View.GONE);
//                viewSeperator.setVisibility(View.GONE);
//            }

            if (normalchatsList.size() > 0) {
                chatsAdapter = new ChatsAdapter(getActivity(), alljsonObjects);
                recyclerView.setAdapter(chatsAdapter);
            } else {
                if (normalchatsList.size() == 0 && pinnedchatsList.size() == 0) {
                    errorLayout.setVisibility(View.VISIBLE);
                }
            }


            ChatsAdapter.selectRemoveItem.clear();
            try {
                ChatsAdapter.clearall();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            chatsAdapter.notifyDataSetChanged();
            counter = 0;
        } catch (Exception e) {

        }
        //selectionList.clear();
    }


//    public void clearActionM() {
//
//        try {
//            ChatsAdapter.is_in_action_mode = false;
//            MainActivity.mainActivity.toolbar.getMenu().clear();
//            MainActivity.mainActivity.toolbar.inflateMenu(R.menu.chat_main);
//            MainActivity.mainActivity.toolbar.setNavigationIcon(null);
//            MainActivity.mainActivity.counterTextView.setText(R.string.app_name);
//            AppController.isPinnedInActionMode = false;
//            AppController.isNormalChatInActionMode = false;
//            //AppController.whichRecylerSelected = 0;
//            if (normalchatsList.size() > 0) {
//
//            } else {
//                if (normalchatsList.size() == 0 && pinnedchatsList.size() == 0) {
//                    errorLayout.setVisibility(View.VISIBLE);
//                }
//            }
//
//
//            Menu menu = MainActivity.mainActivity.toolbar.getMenu();
//            MenuItem menuItem = menu.findItem(R.id.contact_search);
//            SearchView view = (SearchView) MenuItemCompat.getActionView(menuItem);
//
//            EditText searchEditText = (EditText) view.findViewById(android.support.v7.appcompat.R.id.search_src_text);
//            searchEditText.setHintTextColor(getActivity().getResources().getColor(R.color.white));
//
//            view.setOnQueryTextListener(this);
//
//            GetAllChats getAllChats = dbHandler.GetCompleteChats();
//            normalchatsList = getAllChats.getNormalChatList();
//
//            pinnedchatsList = getAllChats.getPinnedChatList();
//
//
//            List<JSONObject> alljsonObjects = new ArrayList<>();
//            alljsonObjects.addAll(pinnedchatsList);
//            alljsonObjects.addAll(normalchatsList);
//
//            chatsAdapter.changeValues(alljsonObjects);
////
////            pinned_chats_list.setVisibility(View.VISIBLE);
////            pinnedChatsAdapter = new PinnedChatsAdapter(getActivity(), pinnedchatsList);
////            pinned_chats_list.setAdapter(pinnedChatsAdapter);
//
//
//            ChatsAdapter.selectRemoveItem.clear();
//            // PinnedChatsAdapter.selectRemoveItem.clear();
//            try {
//                ChatsAdapter.clearall();
//                // PinnedChatsAdapter.clearall();
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//
////            chatsAdapter.notifyDataSetChanged();
//
//
//            //pinnedChatsAdapter.notifyDataSetChanged();
//            counter = 0;
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        //selectionList.clear();
//    }


    public void Refreshfrommain() {

        try {
            ChatsAdapter.is_in_action_mode = false;
            AppController.isPinnedInActionMode = false;
            AppController.isNormalChatInActionMode = false;
            AppController.whichRecylerSelected = 0;
            GetAllChats getAllChats = dbHandler.GetCompleteChats();
            normalchatsList = getAllChats.getNormalChatList();

            pinnedchatsList = getAllChats.getPinnedChatList();


            List<JSONObject> alljsonObjects = new ArrayList<>();
            alljsonObjects.addAll(pinnedchatsList);
            alljsonObjects.addAll(normalchatsList);
//
//            chatsAdapter = new ChatsAdapter(getActivity(), alljsonObjects);
//            recyclerView.setAdapter(chatsAdapter);
            chatsAdapter.changeValues(alljsonObjects);
            ChatsAdapter.selectRemoveItem.clear();
            try {
                ChatsAdapter.clearall();
            } catch (JSONException e) {
                e.printStackTrace();
            }
//            chatsAdapter.notifyDataSetChanged();
            counter = 0;
        } catch (Exception e) {

        }
    }

    private void showalert() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.recovery_email);

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);


        final EditText recovery_email, receovery_phone;
        Button submit;

        recovery_email = (EditText) dialog.findViewById(R.id.recovery_email);
        receovery_phone = (EditText) dialog.findViewById(R.id.recovery_phone);
        submit = (Button) dialog.findViewById(R.id.recovery_submit);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValidEmail(recovery_email.getText().toString())) {
                    if (recovery_email.getText().toString().trim().equalsIgnoreCase("") || receovery_phone.getText().toString().trim().equalsIgnoreCase("")) {
                        Toast.makeText(getActivity(), "Required fields are empty", Toast.LENGTH_SHORT).show();
                    } else {
                        Utils.showSimpleProgressDialog(main_activity, "Please wait. . .", false);
                        new updateemail(recovery_email.getText().toString(), receovery_phone.getText().toString(), dialog).execute();
                    }
                } else {
                    Toast.makeText(main_activity, "Enter Valid email addresses.", Toast.LENGTH_SHORT).show();
                }
            }
        });
        dialog.show();
    }

    private void showalertpass(final int i) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        if (i == 1) {
            dialog.setContentView(R.layout.password_layout_forgot);
        } else {
            dialog.setContentView(R.layout.password_layout);

        }
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);


        final EditText password, confirm_password;
        Button submit;
        password = (EditText) dialog.findViewById(R.id.password);
        confirm_password = (EditText) dialog.findViewById(R.id.confirm_password);
        submit = (Button) dialog.findViewById(R.id.password_submit);


        InputFilter filter = new InputFilter() {
            public CharSequence filter(CharSequence source, int start, int end,
                                       Spanned dest, int dstart, int dend) {
                for (int i = start; i < end; i++) {
                    if (Character.isSpaceChar(source.charAt(i))) {
                        return "";
                    }
                }
                return null;
            }
        };

        password.setFilters(new InputFilter[]{filter});
        confirm_password.setFilters(new InputFilter[]{filter});

        if (i == 1) {
            TextView forgot_pass = (TextView) dialog.findViewById(R.id.pass_forgot);
            forgot_pass.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new hitapiforotp("normal").execute();
                    Utils.showSimpleProgressDialog(main_activity, "Please wait. . .", false);

                    dialog.dismiss();
                }
            });
        }
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!password.getText().toString().equalsIgnoreCase("")) {
                    if (password.getText().toString().equalsIgnoreCase(confirm_password.getText().toString())) {
                        if (i == 0) {
                            try {
                                dbHandler.Updatelock(ChatsAdapter.selectRemoveItem.get(0), "1");
                                dbHandler.UpdatePassword(ChatsAdapter.selectRemoveItem.get(0), password.getText().toString());
                                chatsAdapter.updateKeyAndValue(ChatsAdapter.selectRemoveItem.get(0), "Password", password.getText().toString());
                                chatsAdapter.updateKeyAndValue(ChatsAdapter.selectRemoveItem.get(0), "isLocked", "1");

                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            try {
//                                refresh();
//                                clearActionM();
                                resetToolbarAndCount();
                                dialog.dismiss();
                            } catch (Exception e) {

                            }
                        } else if (i == 3) {

                            try {
                                dbHandler.Updatelock(ChatsAdapter.selectRemoveItem.get(0), "0");
                                dbHandler.UpdatePassword(ChatsAdapter.selectRemoveItem.get(0), "");
                                chatsAdapter.updateKeyAndValue(ChatsAdapter.selectRemoveItem.get(0), "Password", "");
                                chatsAdapter.updateKeyAndValue(ChatsAdapter.selectRemoveItem.get(0), "isLocked", "0");

                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                            try {
//                                ChatFragment.refreshLay();
//                                refresh();
//                                clearActionM();
                                resetToolbarAndCount();

                                dialog.dismiss();
                            } catch (Exception e) {

                            }


                        } else {

                            try {
                                dbHandler.Updatelock(ChatsAdapter.selectRemoveItem.get(0), "0");
                                dbHandler.UpdatePassword(ChatsAdapter.selectRemoveItem.get(0), "");
                                chatsAdapter.updateKeyAndValue(ChatsAdapter.selectRemoveItem.get(0), "Password", "");
                                chatsAdapter.updateKeyAndValue(ChatsAdapter.selectRemoveItem.get(0), "isLocked", "0");

                            } catch (Exception e) {
                                e.printStackTrace();

                            }


                            try {
//                                ChatFragment.refreshLay();
//                                refresh();
//                                clearActionM();
                                resetToolbarAndCount();

                                dialog.dismiss();
                            } catch (Exception e) {

                            }


                        }
                    } else {
                        Toast.makeText(getActivity(), "Password Mismatch. Kindly try again", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getActivity(), "Password Should not be empty. Kindly try again", Toast.LENGTH_SHORT).show();

                }

            }
        });

        if (!dialog.isShowing()) {
            dialog.show();
        }

    }

    private void showOTP(final String otp_val) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.otp_layout);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);


        final EditText otp;
        Button submit;
        otp = (EditText) dialog.findViewById(R.id.otp_edit);

        submit = (Button) dialog.findViewById(R.id.otp_submit);
        otp.setText(otp_val);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String otp_value = otp.getText().toString();

                if (otp_val.equalsIgnoreCase(otp_value)) {
                    showalertpass(3);
                }
                dialog.dismiss();

            }
        });

        if (!dialog.isShowing()) {
            dialog.show();
        }


    }

    private void showunlockotp(final String otp_val) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.otp_layout);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);


        final EditText otp;
        Button submit;
        otp = (EditText) dialog.findViewById(R.id.otp_edit);

        submit = (Button) dialog.findViewById(R.id.otp_submit);
        otp.setText(otp_val);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String otp_value = otp.getText().toString();
                Log.d("onClick: ", "otp:" + otp_value + "," + otp_val);
                if (otp_val.equalsIgnoreCase(otp_value)) {
                    dbHandler.Updatelock(ChatsAdapter.selectRemoveItem.get(0), "0");
                    dbHandler.UpdatePassword(ChatsAdapter.selectRemoveItem.get(0), "");

                    try {
                        chatsAdapter.updateKeyAndValue(ChatsAdapter.selectRemoveItem.get(0), "Password", "");
                        chatsAdapter.updateKeyAndValue(ChatsAdapter.selectRemoveItem.get(0), "isLocked", "0");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    dialog.dismiss();
                }


            }
        });

        if (!dialog.isShowing()) {
            dialog.show();
        }

    }

    private void showalertunlock() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);

        dialog.setContentView(R.layout.password_layout);

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);


        final EditText password, confirm_password;
        Button submit;
        password = (EditText) dialog.findViewById(R.id.password);
        confirm_password = (EditText) dialog.findViewById(R.id.confirm_password);
        submit = (Button) dialog.findViewById(R.id.password_submit);


        InputFilter filter = new InputFilter() {
            public CharSequence filter(CharSequence source, int start, int end,
                                       Spanned dest, int dstart, int dend) {
                for (int i = start; i < end; i++) {
                    if (Character.isSpaceChar(source.charAt(i))) {
                        return "";
                    }
                }
                return null;


            }
        };

        password.setFilters(new InputFilter[]{filter});
        confirm_password.setFilters(new InputFilter[]{filter});

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!password.getText().toString().equalsIgnoreCase("")) {
                    if (password.getText().toString().equalsIgnoreCase(confirm_password.getText().toString())) {
                        dbHandler.Updatelock(ChatsAdapter.selectRemoveItem.get(0), "0");
                        dbHandler.UpdatePassword(ChatsAdapter.selectRemoveItem.get(0), "");

                    } else {
                        Toast.makeText(getActivity(), "Password Mismatch. Kindly try again", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getActivity(), "Password Should not be empty", Toast.LENGTH_SHORT).show();
                }
            }
        });
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    public void createShortCut(String name, String image, String chatRoomType, String ChatRoomId, View childAt, String createBy, String groupImage) {

        try {
            ImageView imageView = (ImageView) childAt.findViewById(R.id.contact_image);
            Intent shortCutInt = new Intent(getActivity(),
                    Splash_activity.class);
            shortCutInt.setAction(Intent.ACTION_MAIN);
            Intent addInt = new Intent();
            addInt.putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortCutInt);
            addInt.putExtra(Intent.EXTRA_SHORTCUT_NAME, name);
//
            Bitmap scaledBitmap = Bitmap.createScaledBitmap(convertImageViewToBitmap(imageView), 128, 128, true);
            addInt.putExtra(Intent.EXTRA_SHORTCUT_ICON, scaledBitmap);


            // Set Install action in Intent
            addInt.setAction("com.android.launcher.action.INSTALL_SHORTCUT");
            shortCutInt.putExtra("appShortcut", "true");
            shortCutInt.putExtra("zoeChatID", ChatRoomId);
            shortCutInt.putExtra("groupId", ChatRoomId);
            shortCutInt.putExtra("create_by", createBy);
            shortCutInt.putExtra("chatRoomType", chatRoomType);
            shortCutInt.putExtra("user_name", name);
            shortCutInt.putExtra("image", image);
            shortCutInt.putExtra("grp_image", groupImage);
            // Broadcast the created intent
            getActivity().sendBroadcast(addInt);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Bitmap convertImageViewToBitmap(ImageView v) {

        Bitmap bm = ((BitmapDrawable) v.getDrawable()).getBitmap();

        return bm;
    }

    private void runOnUiThread(Runnable runnable) {
        handler.post(runnable);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {
            if (resultCode == RESULT_CANCELED) {
                ChatsAdapter.is_in_action_mode = false;
                MainActivity.mainActivity.toolbar.getMenu().clear();
                MainActivity.mainActivity.toolbar.inflateMenu(R.menu.chat_main);
                MainActivity.mainActivity.toolbar.setNavigationIcon(null);
                MainActivity.mainActivity.counterTextView.setText(R.string.app_name);
            }

        }
    }


    public static interface count {
        void getCount(int value);
    }

    private class checkmail extends AsyncTask<String, String, String> implements AsyncTaskCompleteListener {


        String result = "";

        @Override
        protected String doInBackground(String... params) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("id", SharedHelper.getKey(getActivity(), "id"));
                Log.d("doInBackground:", "posting_values:" + jsonObject);

                if (internet) {
                    new PostHelper(Const.Methods.MAIL_CHECK, jsonObject.toString(), Const.ServiceCode.PARTICIPANTS_DETAILS, getActivity(), this);
                } else {
                    result = "yes";
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return result;
        }


        @Override
        public void onTaskCompleted(JSONObject response, int serviceCode) {

            Log.d("onTaskCompleted: ", "" + response);


            if (response.optString("error").equalsIgnoreCase("true")) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Utils.removeProgressDialog();

                        showalert();

                    }
                });

            } else {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Utils.removeProgressDialog();

                        showalertpass(0);
                    }
                });
            }

        }
    }

    private class hitapiforotp extends AsyncTask<String, String, String> implements AsyncTaskCompleteListener {


        String result = "";
        String unlock1;

        public hitapiforotp(String unlock) {
            unlock1 = unlock;
        }

        @Override
        protected String doInBackground(String... params) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("id", SharedHelper.getKey(getActivity(), "id"));
                Log.d("doInBackground: ", "posting_values:" + jsonObject);
                internet = Utils.isNetworkAvailable(getActivity());
                if (internet) {
                    new PostHelper(Const.Methods.OTP_REQUEST, jsonObject.toString(), Const.ServiceCode.PARTICIPANTS_DETAILS, getActivity(), this);
                } else {
                    result = "yes";
                }

            } catch (JSONException e) {

                e.printStackTrace();
            }
            return result;
        }


        @Override
        public void onTaskCompleted(final JSONObject response, int serviceCode) {

            Log.d("OTP_REQUEST:", "" + response);
            if (response.optString("error").equalsIgnoreCase("true")) {

            } else {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (unlock1.equalsIgnoreCase("unlock")) {
                            Utils.removeProgressDialog();
                            showunlockotp(response.optString("otp"));
                        } else {
                            Utils.removeProgressDialog();

                            showOTP(response.optString("otp"));
                        }
                    }
                });
            }

        }
    }

    private class updateemail extends AsyncTask<String, String, String> implements AsyncTaskCompleteListener {


        String result = "";
        String email = "";
        String password = "";
        Dialog dialogView;

        public updateemail(String email_val, String password_val, Dialog dialog) {
            email = email_val;
            password = password_val;
            dialogView = dialog;
        }

        @Override
        protected String doInBackground(String... params) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("id", SharedHelper.getKey(getActivity(), "id"));
                jsonObject.put("email", email);
                jsonObject.put("phone", password);
//                Log.d("doInBackground: ", "posting_values:" + jsonObject);

                if (internet) {
                    new PostHelper(Const.Methods.UPDATE_MAIL, jsonObject.toString(), Const.ServiceCode.PARTICIPANTS_DETAILS, getActivity(), this);
                } else {
                    result = "yes";
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return result;
        }


        @Override


        public void onTaskCompleted(JSONObject response, int serviceCode) {

            Log.d("onTaskCompleted: ", "" + response);


            if (response.optString("error").equalsIgnoreCase("false")) {
                dialogView.dismiss();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Utils.removeProgressDialog();
                        showalertpass(0);
                    }
                });
            }

        }
    }

}
