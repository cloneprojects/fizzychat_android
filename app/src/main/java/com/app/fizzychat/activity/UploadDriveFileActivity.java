package com.app.fizzychat.activity;


import android.Manifest;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.drive.DriveApi;
import com.google.android.gms.drive.DriveClient;
import com.google.android.gms.drive.DriveContents;
import com.google.android.gms.drive.DriveFile;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.DriveResourceClient;
import com.google.android.gms.drive.MetadataBuffer;
import com.google.android.gms.drive.OpenFileActivityOptions;
import com.google.android.gms.drive.query.Filters;
import com.google.android.gms.drive.query.Query;
import com.google.android.gms.drive.query.SearchableField;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.googleapis.extensions.android.gms.auth.GooglePlayServicesAvailabilityIOException;
import com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException;
import com.google.api.client.googleapis.media.MediaHttpDownloader;
import com.google.api.client.googleapis.media.MediaHttpDownloaderProgressListener;
import com.google.api.client.googleapis.media.MediaHttpUploader;
import com.google.api.client.googleapis.media.MediaHttpUploaderProgressListener;
import com.google.api.client.http.FileContent;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.ExponentialBackOff;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;
import com.app.fizzychat.DBHelper.DBHandler;
import com.app.fizzychat.R;
import com.app.fizzychat.baseUtils.AsyncTaskCompleteListener;
import com.app.fizzychat.baseUtils.Const;
import com.app.fizzychat.baseUtils.PostHelper;
import com.app.fizzychat.baseUtils.SharedHelper;
import com.app.fizzychat.baseUtils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class UploadDriveFileActivity extends AppCompatActivity implements AsyncTaskCompleteListener, GoogleApiClient
        .ConnectionCallbacks,
        GoogleApiClient
                .OnConnectionFailedListener,
        EasyPermissions.PermissionCallbacks {

    private static final String BACKUP_FOLDER_KEY = "";
    private static final String TAG = UploadDriveFileActivity.class.getSimpleName();
    private static final int REQUEST_CODE_CAPTURE_IMAGE = 111;
    private static final int COMPLETE_AUTHORIZATION_REQUEST_CODE = 1231;
    Button savebtn, getbtn;
    public static final String LIVE_PATH = "rtsp://118.200.209.211:554/user=test&password=12345&channel=01&stream=0.sdp"; //update package name;
    protected static final int REQUEST_CODE_OPEN_ITEM = 1;

    private DriveClient mDriveClient;
    private DriveResourceClient mDriveResourceClient;
    private GoogleSignInClient mGoogleSignInClient;
    GoogleApiClient mGoogleApiClient;
    DriveId driveId;
    static GoogleAccountCredential mCredential;
    com.google.api.services.drive.Drive service;
    ProgressBar pbProcessing;
    static final int REQUEST_ACCOUNT_PICKER = 1000;
    static final int REQUEST_ACCOUNT_PICKER_INITIAL = 3000;
    static final int REQUEST_AUTHORIZATION = 1001;
    static final int REQUEST_GOOGLE_PLAY_SERVICES = 1002;
    static final int REQUEST_PERMISSION_GET_ACCOUNTS = 1003;
    private static final String PREF_ACCOUNT_NAME = "accountName";
    private static final String[] SCOPES = {DriveScopes.DRIVE_METADATA_READONLY,
            DriveScopes.DRIVE, DriveScopes.DRIVE_FILE,
            DriveScopes.DRIVE_APPDATA, DriveScopes.DRIVE_METADATA,
            DriveScopes.DRIVE_READONLY, DriveScopes.DRIVE_SCRIPTS,
            DriveScopes.DRIVE_PHOTOS_READONLY};
    TextView loadingPercent, localTime, accountNameTextView;
    RelativeLayout loadingLayout;
    AppCompatButton procedBackup;
    LinearLayout accountLayout;
    TextView cloudText,settingsText;
    ImageView cloudDriveImage,settingsIcon;

    public static int getPrimaryCOlor(Context context) {
        final TypedValue value = new TypedValue();
        context.getTheme().resolveAttribute(R.attr.colorPrimary, value, true);
        return value.data;
    }

    public void setIconColour(Drawable drawable) {

        drawable.setColorFilter(new PorterDuffColorFilter(getPrimaryCOlor(UploadDriveFileActivity.this), PorterDuff.Mode.SRC_IN));

    }



    private void Setheme(String themevalue) {
        switch (themevalue) {
            case "1":
                setTheme(R.style.AppThemeGreen);
                break;
            case "2":
                setTheme(R.style.AppThemeBlue);
                break;
            case "3":
                setTheme(R.style.AppThemeIndigo);
                break;
            case "4":
                setTheme(R.style.AppThemeGrey);
                break;
            case "5":
                setTheme(R.style.AppThemeYellow);
                break;
            case "6":
                setTheme(R.style.AppThemeOrange);
                break;
            case "7":
                setTheme(R.style.AppThemePurple);
                break;
            case "8":
                setTheme(R.style.AppThemePaleGreen);
                break;
            case "9":
                setTheme(R.style.AppThemelightBlue);
                break;
            case "10":
                setTheme(R.style.AppThemePink);
                break;
            case "11":
                setTheme(R.style.AppThemelightGreen);
                break;
            case "12":
                setTheme(R.style.AppThemelightRed);
                break;
            default:
                setTheme(R.style.AppThemeGreen);
                break;
        }
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String themevalue = SharedHelper.getKey(this, "theme_value");
        Setheme(themevalue);
        setContentView(R.layout.activity_chat_backup);
        //  savebtn = (Button) findViewById(R.id.savebtn);
        //getbtn = (Button) findViewById(R.id.getbtn);
        pbProcessing = (ProgressBar) findViewById(R.id.pbProcessing);
        loadingLayout = (RelativeLayout) findViewById(R.id.loadingLayout);
        accountLayout = (LinearLayout) findViewById(R.id.accountLayout);
        //downloadStatus = (TextView) findViewById(R.id.downloadStatus);
        localTime = (TextView) findViewById(R.id.localTime);
        loadingPercent = (TextView) findViewById(R.id.loadingPercent);
        cloudText = (TextView) findViewById(R.id.cloudText);
        cloudDriveImage = (ImageView) findViewById(R.id.cloudDriveImage);

        settingsText = (TextView) findViewById(R.id.settingsText);
        settingsIcon = (ImageView) findViewById(R.id.settingsIcon);
        accountNameTextView = (TextView) findViewById(R.id.accountName);
        procedBackup = (AppCompatButton) findViewById(R.id.procedBackup);
        pbProcessing.setMax(100);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                .detectAll()
                .penaltyLog()
                .build();
        StrictMode.setThreadPolicy(policy);
//
        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


        mGoogleSignInClient = buildGoogleSignInClient();


        mCredential = GoogleAccountCredential.usingOAuth2(
                getApplicationContext(), Arrays.asList(SCOPES))
                .setBackOff(new ExponentialBackOff());

        findViewById(R.id.procedBackup).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                loadingLayout.setVisibility(View.VISIBLE);
                procedBackup.setVisibility(View.GONE);

                signIn();
                getResultsFromApi();

            }
        });
        Log.d(TAG, "onCreate: " + SharedHelper.getKey(UploadDriveFileActivity.this, "lastBackupTime"));
        if (SharedHelper.getKey(UploadDriveFileActivity.this, "lastBackupTime").equalsIgnoreCase("0") || SharedHelper.getKey(UploadDriveFileActivity.this, "lastBackupTime").equalsIgnoreCase(""))
            localTime.setText("Local : Never");
        else {
            String lastBackUpTime = Utils.getDateFormatted(Long.parseLong(SharedHelper.getKey(UploadDriveFileActivity.this, "lastBackupTime")));
            localTime.setText("Local :" + lastBackUpTime);
        }


        if (SharedHelper.getKey(UploadDriveFileActivity.this, "googleDriveAccount").equalsIgnoreCase("0") || SharedHelper.getKey(UploadDriveFileActivity.this, "googleDriveAccount").equalsIgnoreCase(""))
            accountNameTextView.setText("Select Account");
        else
            accountNameTextView.setText(SharedHelper.getKey(UploadDriveFileActivity.this, "googleDriveAccount"));


//        localTime.setText(SharedHelper.getKey(UploadDriveFileActivity.this,"localGoogleTime"));


        accountLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                selectAccount();
            }
        });


        setIconColour(cloudDriveImage.getDrawable());
        setIconColour(settingsIcon.getDrawable());

        settingsText.setTextColor(getPrimaryCOlor(UploadDriveFileActivity.this));
        cloudText.setTextColor(getPrimaryCOlor(UploadDriveFileActivity.this));


    }

    private void selectAccount() {
        if (!isGooglePlayServicesAvailable()) {
            acquireGooglePlayServices();
        } else if (!isDeviceOnline()) {
            Log.e(TAG, "getResultsFromApi: No network connection available.");
        } else {
            chooseAccountInitially();
        }
    }

    private void getResultsFromApi() {
        if (!isGooglePlayServicesAvailable()) {
            acquireGooglePlayServices();
        } else if (mCredential.getSelectedAccountName() == null) {
            chooseAccount();
        } else if (!isDeviceOnline()) {
            Log.e(TAG, "getResultsFromApi: No network connection available.");
        } else {
            new MakeRequestTask(mCredential).execute();
        }
    }

    @AfterPermissionGranted(REQUEST_PERMISSION_GET_ACCOUNTS)
    private void chooseAccount() {
        if (EasyPermissions.hasPermissions(
                this, Manifest.permission.GET_ACCOUNTS)) {
            String accountName = getPreferences(Context.MODE_PRIVATE)
                    .getString(PREF_ACCOUNT_NAME, null);
            if (accountName != null) {
                mCredential.setSelectedAccountName(accountName);
                getResultsFromApi();
            } else {
                // Start a dialog from which the user can choose an account
                startActivityForResult(
                        mCredential.newChooseAccountIntent(),
                        REQUEST_ACCOUNT_PICKER);
            }
        } else {
            // Request the GET_ACCOUNTS permission via a user dialog
            EasyPermissions.requestPermissions(
                    this,
                    "This app needs to access your Google account (via Contacts).",
                    REQUEST_PERMISSION_GET_ACCOUNTS,
                    Manifest.permission.GET_ACCOUNTS, Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE);
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }



    @AfterPermissionGranted(REQUEST_PERMISSION_GET_ACCOUNTS)
    private void chooseAccountInitially() {
        if (EasyPermissions.hasPermissions(
                this, Manifest.permission.GET_ACCOUNTS)) {
            String accountName = getPreferences(Context.MODE_PRIVATE)
                    .getString(PREF_ACCOUNT_NAME, null);

            // Start a dialog from which the user can choose an account
            startActivityForResult(
                    mCredential.newChooseAccountIntent(),
                    REQUEST_ACCOUNT_PICKER_INITIAL);

        } else {
            // Request the GET_ACCOUNTS permission via a user dialog
            EasyPermissions.requestPermissions(
                    this,
                    "This app needs to access your Google account (via Contacts).",
                    REQUEST_PERMISSION_GET_ACCOUNTS,
                    Manifest.permission.GET_ACCOUNTS, Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE);
        }
    }


    @Override
    protected void onPause() {
        if (mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();
        }
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addApi(Drive.API)
                    .addScope(Drive.SCOPE_FILE)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .build();
        }
        mGoogleApiClient.connect();
    }

    private static final String KEY_TEXT_REPLY = "key_text_reply";

    private void inlineReply() {
//        try {
//            // Try to perform a Drive API request, for instance:
        java.io.File folder = Environment.getExternalStorageDirectory();
        String fileName = folder.getPath() + "/Spector/zoechattest.pdf";
        java.io.File myFile = new java.io.File(fileName);

//        File file = service.files().create(myFile).execute();
//        } catch (UserRecoverableAuthIOException e) {
//            startActivityForResult(e.getIntent(), COMPLETE_AUTHORIZATION_REQUEST_CODE);
//        }
//
    }


    private void open() {
//        mProgressBar.setProgress(0);
        DriveFile.DownloadProgressListener listener = new DriveFile.DownloadProgressListener() {
            @Override
            public void onProgress(long bytesDownloaded, long bytesExpected) {
                // Update progress dialog with the latest progress.
                int progress = (int) (bytesDownloaded * 100 / bytesExpected);
                Log.d(TAG, String.format("Loading progress: %d percent", progress));


//                mProgressBar.setProgress(progress);
            }
        };
        DriveFile driveFile = driveId.asDriveFile();
        driveFile.open(mGoogleApiClient, DriveFile.MODE_READ_ONLY, listener)
                .setResultCallback(driveContentsCallback);
        driveId = null;
    }


    private final ResultCallback<DriveApi.DriveContentsResult> driveContentsCallback =
            new ResultCallback<DriveApi.DriveContentsResult>() {
                @Override
                public void onResult(@NonNull DriveApi.DriveContentsResult result) {
                    if (!result.getStatus().isSuccess()) {
                        Log.e(TAG, "Error while opening the file contents");
                        return;
                    }
                    Log.e(TAG, "File contents opened");

                    // Read from the input stream an print to LOGCAT
                    DriveContents driveContents = result.getDriveContents();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(driveContents.getInputStream()));
                    StringBuilder builder = new StringBuilder();
                    String line;
                    try {
                        while ((line = reader.readLine()) != null) {
                            builder.append(line);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    String contentsAsString = builder.toString();
                    Log.e(TAG, contentsAsString);

                    // Close file contents
                    driveContents.discard(mGoogleApiClient);
                }
            };


    private Task<DriveId> pickTextFile() {
        OpenFileActivityOptions openOptions =
                new OpenFileActivityOptions.Builder()
                        .setSelectionFilter(Filters.eq(SearchableField.MIME_TYPE,
                                "text/plain"))
                        .setActivityTitle(getString(R.string.select_file))
                        .build();
        return pickItem(openOptions);
    }


    private Task<DriveId> pickItem(OpenFileActivityOptions openOptions) {
        TaskCompletionSource<DriveId> mOpenItemTaskSource = new TaskCompletionSource<>();
        mDriveClient
                .newOpenFileActivityIntentSender(openOptions)
                .continueWith(new Continuation<IntentSender, Void>() {
                    @Override
                    public Void then(@NonNull Task<IntentSender> task) throws Exception {
                        startIntentSenderForResult(
                                task.getResult(), REQUEST_CODE_OPEN_ITEM, null, 0, 0, 0);
                        return null;
                    }
                });
        return mOpenItemTaskSource.getTask();
    }

    private void listFiles() {
        Query query = new Query.Builder()
                .addFilter(Filters.eq(SearchableField.MIME_TYPE, "text/plain"))
                .build();
        // [START query_files]
        Task<MetadataBuffer> queryTask = mDriveResourceClient.query(query);
        // [END query_files]
        // [START query_results]
        queryTask
                .addOnSuccessListener(this,
                        new OnSuccessListener<MetadataBuffer>() {
                            @Override
                            public void onSuccess(MetadataBuffer metadataBuffer) {
                                // Handle results...
                                // [START_EXCLUDE]
                                Log.e(TAG, "onSuccess: " + metadataBuffer.getCount());
//                                mResultsAdapter.append(metadataBuffer);
                                // [END_EXCLUDE]
                            }
                        })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        // Handle failure...
                        // [START_EXCLUDE]
                        Log.e(TAG, "Error retrieving files", e);
//                        showMessage(getString(R.string.query_failed));
                        finish();
                        // [END_EXCLUDE]
                    }
                });
        // [END query_results]
    }


    private GoogleSignInClient buildGoogleSignInClient() {
        GoogleSignInOptions signInOptions =
                new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                        .requestScopes(Drive.SCOPE_FILE)
                        .build();
        return GoogleSignIn.getClient(this, signInOptions);
    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    protected void signIn() {
        Set<Scope> requiredScopes = new HashSet<>(2);
        requiredScopes.add(Drive.SCOPE_FILE);
        requiredScopes.add(Drive.SCOPE_APPFOLDER);
        GoogleSignInAccount signInAccount = GoogleSignIn.getLastSignedInAccount(this);
        if (signInAccount != null && signInAccount.getGrantedScopes().containsAll(requiredScopes)) {
            initializeDriveClient(signInAccount);
        } else {
            GoogleSignInOptions signInOptions =
                    new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                            .requestScopes(Drive.SCOPE_FILE)
                            .requestScopes(Drive.SCOPE_APPFOLDER)
                            .build();
            GoogleSignInClient googleSignInClient = GoogleSignIn.getClient(this, signInOptions);
            startActivityForResult(googleSignInClient.getSignInIntent(), REQUEST_CODE_CAPTURE_IMAGE);
        }
    }


    private void initializeDriveClient(GoogleSignInAccount signInAccount) {
        mDriveClient = Drive.getDriveClient(getApplicationContext(), signInAccount);
        mDriveResourceClient = Drive.getDriveResourceClient(getApplicationContext(), signInAccount);
//        onDriveClientReady();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(
                requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> list) {
        // Do nothing.
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> list) {
        // Do nothing.
    }

    private boolean isDeviceOnline() {
        ConnectivityManager connMgr =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }

    private boolean isGooglePlayServicesAvailable() {
        GoogleApiAvailability apiAvailability =
                GoogleApiAvailability.getInstance();
        final int connectionStatusCode =
                apiAvailability.isGooglePlayServicesAvailable(this);
        return connectionStatusCode == ConnectionResult.SUCCESS;
    }

    private void acquireGooglePlayServices() {
        GoogleApiAvailability apiAvailability =
                GoogleApiAvailability.getInstance();
        final int connectionStatusCode =
                apiAvailability.isGooglePlayServicesAvailable(this);
        if (apiAvailability.isUserResolvableError(connectionStatusCode)) {
            showGooglePlayServicesAvailabilityErrorDialog(connectionStatusCode);
        }
    }

    void showGooglePlayServicesAvailabilityErrorDialog(
            final int connectionStatusCode) {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        Dialog dialog = apiAvailability.getErrorDialog(
                UploadDriveFileActivity.this,
                connectionStatusCode,
                REQUEST_GOOGLE_PLAY_SERVICES);
        dialog.show();
    }

    private class MakeRequestTask extends AsyncTask<Void, Void, List<String>> {
        com.google.api.services.drive.Drive mService = null;
        private Exception mLastError = null;
        GoogleAccountCredential credential;

        MakeRequestTask(GoogleAccountCredential credential) {
            this.credential = credential;
            HttpTransport transport = AndroidHttp.newCompatibleTransport();
            JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();
            mService = new com.google.api.services.drive.Drive.Builder(
                    transport, jsonFactory, credential)
                    .setApplicationName("Zoi Files")
                    .build();

        }

        private File uploadFile(boolean useDirectUpload) throws IOException {

            service = new com.google.api.services.drive.Drive.Builder(AndroidHttp.newCompatibleTransport(), new GsonFactory(),
                    credential).setApplicationName("ZoeFile").build();

            File fileMetadata = new File();
            fileMetadata.setName("Zeochat");
            fileMetadata.setMimeType("application/x-sqlite3");
            fileMetadata.setDescription("This is a sample pdf file");
            fileMetadata.setParents(Collections.singletonList("appDataFolder"));


            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    loadingPercent.setText(String.valueOf("40%"));
                }
            });
            java.io.File direct = new java.io.File(Environment.getExternalStorageDirectory()
                    + "/Spector");
            if (!direct.exists()) {
                direct.mkdirs();
            }


            String googleDriveFileId = SharedHelper.getKey(UploadDriveFileActivity.this, "googleDriveFileId");
            Log.e(TAG, "googleDriveFileId:" + googleDriveFileId);


            com.google.api.services.drive.Drive.Files.Get upd = mService.files().get(googleDriveFileId);
            Log.e(TAG, "uploadFileId: " + upd.getFileId());
            File updfile = null;
            if (!upd.getFileId().equalsIgnoreCase("")) {
                try {
                    upd.execute();
                    updfile = new File();
                    updfile.setTrashed(true);
                    updfile.setDescription("This is updated description");
                    updfile.setName("Zeochat");
                    updfile.setMimeType("application/x-sqlite3");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }


            String currentDBPath = "//data//" + "//data//" + getPackageName()
                    + "//databases//" + DBHandler.DATABASE_NAME;

            java.io.File myFile = new java.io.File(currentDBPath);

            FileContent mediaContent = new FileContent("application/x-sqlite3", myFile);

            Log.e(TAG, "uploadFile: " + updfile);


            if (updfile == null) {
                Log.e(TAG, "uploadFile:" + "InInsertSection");
                com.google.api.services.drive.Drive.Files.Create insert = service.files().create
                        (fileMetadata,
                                mediaContent);
                MediaHttpUploader uploader = insert.getMediaHttpUploader();
                uploader.setDirectUploadEnabled(false);
                uploader.setChunkSize(MediaHttpUploader.MINIMUM_CHUNK_SIZE);
                //uploader.setProgressListener(new FileUploadProgressListener());

                uploader.setProgressListener(new MediaHttpUploaderProgressListener() {
                    @Override
                    public void progressChanged(MediaHttpUploader uploader) throws IOException {
                        Log.e(TAG, "progressValue:" + uploader.getProgress());

                        if (uploader.getProgress() == 0.0) {


                            UploadDriveFileActivity.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    pbProcessing.setProgress(50);
                                    loadingPercent.setText(String.valueOf("70%"));
                                }
                            });
                        } else if (uploader.getProgress() == 1.0) {


                            UploadDriveFileActivity.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    SharedHelper.putKey(UploadDriveFileActivity.this, "lastBackupTime", "" + System.currentTimeMillis());


                                    loadingPercent.setText(String.valueOf("Completed"));


                                    DBHandler dbHandler = new DBHandler(UploadDriveFileActivity.this);
                                    int totalCount = dbHandler.getTotalMessages();

                                    try {
                                        JSONObject jsonObject = new JSONObject();
                                        jsonObject.put("zoechatid", SharedHelper.getKey(UploadDriveFileActivity.this, "my_zoe_id"));
                                        jsonObject.put("accountName", SharedHelper.getKey(UploadDriveFileActivity.this, "googleDriveAccount"));
                                        jsonObject.put("isDriveLogin", "true");
                                        jsonObject.put("time", System.currentTimeMillis());
                                        jsonObject.put("driveId", SharedHelper.getKey(UploadDriveFileActivity.this, "googleDriveFileId"));
                                        jsonObject.put("messageCount", totalCount);

                                        Log.e("getting_ph", jsonObject.toString());
                                        new PostHelper(Const.Methods.DRIVE_UPDATE, jsonObject.toString(), Const.ServiceCode.DRIVE_UPDATE, UploadDriveFileActivity.this, UploadDriveFileActivity.this);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        //Utils.removeProgressDialog();
                                    }

                                }
                            });


                        }

                    }
                });
                return insert.execute();
            } else {
//                mService.files().delete("1ilxEU1IJTdodWMOLurKPg40o6KTUHwOuuZQGvQHCsTwm7Q0a3w").execute();

                Log.e(TAG, "uploadFile:" + "InUpdateSection");
                String googleDriveFileIdUpdate = SharedHelper.getKey(UploadDriveFileActivity.this, "googleDriveFileId");
                Log.e(TAG, "googleDriveFileIdUpdate:" + googleDriveFileIdUpdate);
                com.google.api.services.drive.Drive.Files.Update update = service.files()
                        .update(googleDriveFileIdUpdate, updfile, mediaContent);
                MediaHttpUploader uploader = update.getMediaHttpUploader();
                uploader.setDirectUploadEnabled(false);
                uploader.setProgressListener(new MediaHttpUploaderProgressListener() {
                    @Override
                    public void progressChanged(MediaHttpUploader uploader) throws IOException {
                        Log.e(TAG, "progressValue:" + uploader.getProgress());

                        if (uploader.getProgress() == 0.0) {


                            UploadDriveFileActivity.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    pbProcessing.setProgress(50);
                                    loadingPercent.setText(String.valueOf("50%"));
                                }
                            });
                        } else if (uploader.getProgress() == 1.0) {


                            UploadDriveFileActivity.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    SharedHelper.putKey(UploadDriveFileActivity.this, "lastBackupTime", "" + System.currentTimeMillis());

                                    loadingLayout.setVisibility(View.GONE);
                                    procedBackup.setVisibility(View.VISIBLE);
                                    loadingPercent.setText(String.valueOf("100%"));
                                    String lastBackUpTime = Utils.getDateFormatted(System.currentTimeMillis());

                                    Log.e("ass", "lastBackUpTime:" + lastBackUpTime);
                                    localTime.setText("Local :" + lastBackUpTime);
                                    pbProcessing.setProgress(100);

                                    DBHandler dbHandler = new DBHandler(UploadDriveFileActivity.this);
                                    int totalCount = dbHandler.getTotalMessages();

                                    try {
                                        JSONObject jsonObject = new JSONObject();
                                        jsonObject.put("zoechatid", SharedHelper.getKey(UploadDriveFileActivity.this, "my_zoe_id"));
                                        jsonObject.put("accountName", SharedHelper.getKey(UploadDriveFileActivity.this, "googleDriveAccount"));
                                        jsonObject.put("isDriveLogin", "true");
                                        jsonObject.put("time", System.currentTimeMillis());
                                        jsonObject.put("driveId", SharedHelper.getKey(UploadDriveFileActivity.this, "googleDriveFileId"));
                                        jsonObject.put("messageCount", totalCount);

                                        Log.e("getting_ph", jsonObject.toString());
                                        new PostHelper(Const.Methods.DRIVE_UPDATE, jsonObject.toString(), Const.ServiceCode.DRIVE_UPDATE, UploadDriveFileActivity.this, UploadDriveFileActivity.this);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        //Utils.removeProgressDialog();
                                    }

                                }
                            });


                        }

                    }
                });
                return update.execute();
            }

//            uploader.setProgressListener(new FileUploadProgressListener());

        }


        public class FileUploadProgressListener implements MediaHttpUploaderProgressListener {

            public void progressChanged(MediaHttpUploader uploader) throws IOException {
                double i = uploader.getProgress();
                Log.e(TAG, "valuesis:" + i);
                if (i == 0.0) {// postToDialog("Initiation Started");
                    //downloadStatus.setText("Upload Started");

                    pbProcessing.setProgress(50);
//                    downloadStatus.setText("50%");
                    Log.e(TAG, "progressChanged:" + "0to20");
                } else if (i == 1.0) {// postToDialog("Initiation Completed");
                    pbProcessing.setMax(100);
                    pbProcessing.setProgress(100);
                    //downloadStatus.setText("100%");
                    pbProcessing.setVisibility(View.GONE);
                    Log.e(TAG, "progressChanged:" + "20to40");
                }
            }
        }

        private void downloadFile(boolean useDirectDownload, File uploadedFile)
                throws IOException {

//            service = new com.google.api.services.drive.Drive.Builder(AndroidHttp.newCompatibleTransport(), new GsonFactory(),
//                    credential).setApplicationName("ZoeFile").build();

            java.io.File direct = new java.io.File(Environment.getExternalStorageDirectory()
                    + "/Spectors");
            if (!direct.exists()) {
                direct.mkdirs();
            }

            java.io.File parentDir = new java.io.File(direct.getAbsolutePath());
            if (!parentDir.exists() && !parentDir.mkdirs()) {
                throw new IOException("Unable to create parent directory");
            }
            OutputStream out = new FileOutputStream(new java.io.File(parentDir, uploadedFile.getName() + ".pdf"));


            com.google.api.services.drive.Drive.Files.Get get = mService.files().get(uploadedFile.getId());
            get.getMediaHttpDownloader().setProgressListener(new DownloadProgressListener());
            get.executeMediaAndDownloadTo(out);


            com.google.api.services.drive.Drive.Files.Export files = mService.files().export
                    (uploadedFile.getId(),
                            uploadedFile.getMimeType() == null ? "application/zip" : uploadedFile.getMimeType());


            Log.e(TAG, "downloadFile: " + files.toString() + "  " + files.getLastResponseHeaders() +
                    "\n" + files.getDisableGZipContent() + "\n" + files.getFields());

            MediaHttpDownloader downloader =
                    new MediaHttpDownloader(new NetHttpTransport(), mService.getRequestFactory()
                            .getInitializer());
            downloader.setDirectDownloadEnabled(false);

            downloader.setProgressListener(new MediaHttpDownloaderProgressListener() {
                @Override
                public void progressChanged(MediaHttpDownloader downloader) throws IOException {
                    Log.e(TAG, "downProgress:" + downloader.getProgress());
                }
            });


            Log.e(TAG, "downloadFile: " + out + "\n" + uploadedFile
                    .getThumbnailLink()
                    + "\n" + mService.files().export(uploadedFile.getId(), uploadedFile
                    .getMimeType()) +
                    "\n" +
                    downloader.getTransport().createRequestFactory().getInitializer());
        }


        /**
         * Background task to call Drive API.
         *
         * @param params no parameters needed for this task.
         */
        @Override
        protected List<String> doInBackground(Void... params) {
            try {
                File file = uploadFile(false);

                Log.e(TAG, "myFileIdIs: " + file.getId());

                SharedHelper.putKey(UploadDriveFileActivity.this, "googleDriveFileId", file.getId());

//                downloadFile(true, file);
                return getDataFromApi();
            } catch (Exception e) {
                mLastError = e;
                cancel(true);
                return null;
            }
        }

        /**
         * Fetch a list of up to 10 file names and IDs.
         *
         * @return List of Strings describing files, or an empty list if no files
         * found.
         * @throws IOException
         */
        private List<String> getDataFromApi() throws IOException {
            // Get a list of up to 10 files.
            List<String> fileInfo = new ArrayList<String>();
            FileList lst = mService.files().list().setSpaces("appDataFolder").execute();
            List<File> fs = lst.getFiles();
            if (lst != null) {
                for (File fil : fs) {
                    Log.e(TAG, "listOfFilesAre: " + fil + "\n" + fil.getId().getBytes() + "\n" + fil.getTeamDriveId());
                    // Log.e(TAG, "listOfFilesAre: " + fil + "\n" + fil.getWebViewLink() + "\n" + fil.getSize());
                    //   downloadFile(false, fil);
                }
            }
            FileList result = mService.files().list()
                    .setSpaces("appDataFolder")
                    .setFields("nextPageToken, files(id, name, mimeType)")
                    .execute();
            List<File> files = result.getFiles();
            if (files != null) {
                for (File file : files) {
                    fileInfo.add(String.format("%s (%s)\n",
                            file.getName(), file.getId()));
                }
            }
            return fileInfo;
        }


        @Override
        protected void onPreExecute() {
//            mOutputText.setText("");
//            mProgress.show();
        }

        @Override
        protected void onPostExecute(List<String> output) {
//            mProgress.hide();
            if (output == null || output.size() == 0) {
//                mOutputText.setText("No results returned.");
                Log.e(TAG, "onPostExecute: No Result Returned");
            } else {
                output.add(0, "Data retrieved using the Drive API:");
                Log.e(TAG, "onPostExecute: " + TextUtils.join("\n", output));
//                mOutputText.setText(TextUtils.join("\n", output));
            }
        }

        @Override
        protected void onCancelled() {
//            mProgress.hide();
            if (mLastError != null) {
                if (mLastError instanceof GooglePlayServicesAvailabilityIOException) {
                    showGooglePlayServicesAvailabilityErrorDialog(
                            ((GooglePlayServicesAvailabilityIOException) mLastError)
                                    .getConnectionStatusCode());
                } else if (mLastError instanceof UserRecoverableAuthIOException) {
                    startActivityForResult(
                            ((UserRecoverableAuthIOException) mLastError).getIntent(),
                            UploadDriveFileActivity.REQUEST_AUTHORIZATION);
                } else {
                    Log.e(TAG, "onCancelled: ", mLastError);
                    Log.e(TAG, "onCancelled: " + mLastError.getMessage());
//                    mOutputText.setText("The following error occurred:\n"
//                            + mLastError.getMessage());
                }
            } else {
                Log.e(TAG, "onCancelled: Cancel");
//                mOutputText.setText("Request cancelled.");
            }
        }
    }


    //custom listener for download progress
    class DownloadProgressListener implements MediaHttpDownloaderProgressListener {


        @Override
        public void progressChanged(MediaHttpDownloader downloader) throws IOException {


            Log.e("downloader", "dxs" + downloader.getProgress());

            switch (downloader.getDownloadState()) {

                //Called when file is still downloading
                //ONLY CALLED AFTER A CHUNK HAS DOWNLOADED,SO SET APPROPRIATE CHUNK SIZE
                case MEDIA_IN_PROGRESS:
                    //Add code for showing progress
                    Log.e("checkDownload", "InProgress:Cancel");
                    break;
                //Called after download is complete
                case MEDIA_COMPLETE:
                    //Add code for download completion
                    Log.e("checkDownload", "completed:Cancel");
                    break;
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_CODE_CAPTURE_IMAGE:
                if (resultCode != RESULT_OK) {
                    // Sign-in may fail or be cancelled by the user. For this sample, sign-in is
                    // required and is fatal. For apps where sign-in is optional, handle
                    // appropriately
                    Log.e(TAG, "Sign-in failed.");
                    finish();
                    return;
                }

                Task<GoogleSignInAccount> getAccountTask =
                        GoogleSignIn.getSignedInAccountFromIntent(data);
                if (getAccountTask.isSuccessful()) {
                    initializeDriveClient(getAccountTask.getResult());
                } else {
                    Log.e(TAG, "Sign-in failed.");
                    finish();
                }
                break;
            case REQUEST_CODE_OPEN_ITEM:
                if (resultCode == RESULT_OK) {
                    driveId = data.getParcelableExtra(
                            OpenFileActivityOptions.EXTRA_RESPONSE_DRIVE_ID);
                    Log.e(TAG, "onActivityResult: " + driveId);

                    open();


//                    mOpenItemTaskSource.setResult(driveId);
                } else {
                    Log.e(TAG, "onActivityResult: unable to open ");
                }
                break;

            case COMPLETE_AUTHORIZATION_REQUEST_CODE:
                if (resultCode == Activity.RESULT_OK) {
                    Log.e(TAG, "onActivityResult: auth");
                    // App is authorized, you can go back to sending the API request
//                    inlineReply();
                } else {
                    // User denied access, show him the account chooser again
                    Log.e(TAG, "onActivityResult: denied");
                }
                break;

            case REQUEST_GOOGLE_PLAY_SERVICES:
                if (resultCode != RESULT_OK) {
//                    mOutputText.setText(
//                            "This app requires Google Play Services. Please install " +
//                                    "Google Play Services on your device and relaunch this app.");
                    Log.e(TAG, "onActivityResult: require google play");
                } else {
                    getResultsFromApi();
                }
                break;
            case REQUEST_ACCOUNT_PICKER:
                if (resultCode == RESULT_OK && data != null &&
                        data.getExtras() != null) {
                    String accountName =
                            data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);

                    SharedHelper.putKey(UploadDriveFileActivity.this, "googleDriveAccount", accountName);

                    Log.e(TAG, "onActivityResult:" + accountName);
                    accountNameTextView.setText(accountName);

                    if (accountName != null) {
                        SharedPreferences settings =
                                getPreferences(Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = settings.edit();
                        editor.putString(PREF_ACCOUNT_NAME, accountName);
                        editor.apply();
                        mCredential.setSelectedAccountName(accountName);
                        getResultsFromApi();
                    }
                } else {

                    loadingLayout.setVisibility(View.GONE);
                    procedBackup.setVisibility(View.VISIBLE);
                }
                break;

            case REQUEST_ACCOUNT_PICKER_INITIAL:
                if (resultCode == RESULT_OK && data != null &&
                        data.getExtras() != null) {
                    String accountName =
                            data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);

                    SharedHelper.putKey(UploadDriveFileActivity.this, "googleDriveAccount", accountName);

                    Log.e(TAG, "onActivityResult:" + accountName);
                    accountNameTextView.setText(accountName);

                    if (accountName != null) {
                        SharedPreferences settings =
                                getPreferences(Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = settings.edit();
                        editor.putString(PREF_ACCOUNT_NAME, accountName);
                        editor.apply();
                        mCredential.setSelectedAccountName(accountName);
                        // getResultsFromApi();
                    }
                }
                break;


            case REQUEST_AUTHORIZATION:
                if (resultCode == RESULT_OK) {
                    getResultsFromApi();
                }
                break;

        }


        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    @Override
    public void onTaskCompleted(JSONObject response, int serviceCode) {
        switch (serviceCode) {

            case Const.ServiceCode.DRIVE_UPDATE:
                Log.e("response", "" + response);
                if (response != null) {
                    if (response.optString("error").equalsIgnoreCase("false")) {

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        loadingLayout.setVisibility(View.GONE);
                                        procedBackup.setVisibility(View.VISIBLE);
                                        String lastBackUpTime = Utils.getDateFormatted(System.currentTimeMillis());

                                        Log.e("ass", "lastBackUpTime:" + lastBackUpTime);
                                        localTime.setText("Local :" + lastBackUpTime);
                                        pbProcessing.setProgress(100);



                                    }
                                }, 600);


                            }
                        });

                        SharedHelper.putKey(UploadDriveFileActivity.this, "isDriveLogin", "true");
                        SharedHelper.putKey(UploadDriveFileActivity.this, "accountName", response.optString("accountName"));
                        SharedHelper.putKey(UploadDriveFileActivity.this, "driveBackUpTime", response.optString("time"));


                    } else {

                    }
                } else {
                    Utils.showLongToast(getResources().getString(R.string.server_error), getApplicationContext());
                }

                break;
        }

    }
}

