package com.app.fizzychat.activity;

import android.Manifest;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.drive.DriveApi;
import com.google.android.gms.drive.DriveClient;
import com.google.android.gms.drive.DriveContents;
import com.google.android.gms.drive.DriveFile;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.DriveResourceClient;
import com.google.android.gms.drive.MetadataBuffer;
import com.google.android.gms.drive.OpenFileActivityOptions;
import com.google.android.gms.drive.query.Filters;
import com.google.android.gms.drive.query.Query;
import com.google.android.gms.drive.query.SearchableField;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.googleapis.extensions.android.gms.auth.GooglePlayServicesAvailabilityIOException;
import com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException;
import com.google.api.client.googleapis.media.MediaHttpDownloader;
import com.google.api.client.googleapis.media.MediaHttpDownloaderProgressListener;
import com.google.api.client.googleapis.media.MediaHttpUploader;
import com.google.api.client.googleapis.media.MediaHttpUploaderProgressListener;
import com.google.api.client.http.FileContent;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.ExponentialBackOff;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;
import com.app.fizzychat.DBHelper.DBHandler;
import com.app.fizzychat.R;
import com.app.fizzychat.baseUtils.AsyncTaskCompleteListener;
import com.app.fizzychat.baseUtils.Const;
import com.app.fizzychat.baseUtils.PostHelper;
import com.app.fizzychat.baseUtils.SharedHelper;
import com.app.fizzychat.baseUtils.Utils;
import com.app.fizzychat.models.ChatMessages;
import com.app.fizzychat.models.ChatType;
import com.app.fizzychat.models.ChatsMessagesModel;
import com.app.fizzychat.models.ChatsModel;
import com.app.fizzychat.models.ContactsModel;
import com.app.fizzychat.models.GetUserFromDBModel;
import com.app.fizzychat.models.GroupParticiapntsModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class DownloadGoogleDriveFIle extends AppCompatActivity implements AsyncTaskCompleteListener, GoogleApiClient
        .ConnectionCallbacks,
        GoogleApiClient
                .OnConnectionFailedListener,
        EasyPermissions.PermissionCallbacks {

    private static final String BACKUP_FOLDER_KEY = "";
    private static final String TAG = DownloadGoogleDriveFIle.class.getSimpleName();
    private static final int REQUEST_CODE_CAPTURE_IMAGE = 111;
    private static final int COMPLETE_AUTHORIZATION_REQUEST_CODE = 1231;
    SharedPreferences sharedPref;
    SharedPreferences.Editor editor;
    Button savebtn, getbtn;
    public static final String LIVE_PATH = "rtsp://118.200.209.211:554/user=test&password=12345&channel=01&stream=0.sdp"; //update package name;
    protected static final int REQUEST_CODE_OPEN_ITEM = 1;

    private DriveClient mDriveClient;
    private DriveResourceClient mDriveResourceClient;
    private GoogleSignInClient mGoogleSignInClient;
    GoogleApiClient mGoogleApiClient;
    DriveId driveId;
    static GoogleAccountCredential mCredential;
    String accountName;
    com.google.api.services.drive.Drive service;
    ProgressBar pbProcessing;
    static final int REQUEST_ACCOUNT_PICKER = 1000;
    static final int REQUEST_AUTHORIZATION = 1001;
    static final int REQUEST_GOOGLE_PLAY_SERVICES = 1002;
    static final int REQUEST_PERMISSION_GET_ACCOUNTS = 1003;
    private static final String PREF_ACCOUNT_NAME = "accountName";
    private static final String[] SCOPES = {DriveScopes.DRIVE_METADATA_READONLY,
            DriveScopes.DRIVE, DriveScopes.DRIVE_FILE,
            DriveScopes.DRIVE_APPDATA, DriveScopes.DRIVE_METADATA,
            DriveScopes.DRIVE_READONLY, DriveScopes.DRIVE_SCRIPTS,
            DriveScopes.DRIVE_PHOTOS_READONLY};
    //TextView downloadStatus;
    private JSONObject responseObj;
    String driveIdVar, driveLoginTime, isDriveLogin, messageCount;
    private ImageView progressGif;
    LinearLayout doneLinear, restoreLinear;
    TextView restoringText, restoredMessages, timeAgo, restoreContent;
    AppCompatButton nextScreen;
    private String sign = "", id = "", status = "", name = "", image = "", my_zoe_id = "", responseStr;

    RelativeLayout progressGifLinear;
    private long final_percentage;
    private List<GetUserFromDBModel> contact_list;
    private Dialog dialog, dialogGroup;
    private TextView percentage_text, title;
    private boolean doGroupSync = false;
    JSONObject jsonObjectWhole;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_download_google_drive_file);
//        savebtn = (Button) findViewById(R.id.savebtn);
//        getbtn = (Button) findViewById(R.id.getbtn);
        pbProcessing = (ProgressBar) findViewById(R.id.pbProcessing);

        restoringText = (TextView) findViewById(R.id.restoringText);
        timeAgo = (TextView) findViewById(R.id.timeAgo);
        nextScreen = (AppCompatButton) findViewById(R.id.nextScreen);
        restoredMessages = (TextView) findViewById(R.id.restoredMessages);
        restoreContent = (TextView) findViewById(R.id.restoreContent);

        restoreLinear = (LinearLayout) findViewById(R.id.restoreLinear);
        doneLinear = (LinearLayout) findViewById(R.id.doneLinear);
        progressGifLinear = (RelativeLayout) findViewById(R.id.progressGifLinear);

//        downloadStatus = (TextView) findViewById(R.id.downloadStatus);
        pbProcessing.setMax(100);
        progressGif = (ImageView) findViewById(R.id.progressGif);
        GlideDrawableImageViewTarget imageViewTarget = new GlideDrawableImageViewTarget(progressGif);
        // Glide.with(GettingPhoneNumber_activity.this).load("").placeholder(getResources().getDrawable(R.drawable.progress)).into(progressGif);
        Glide.with(DownloadGoogleDriveFIle.this).load(R.drawable.progress).into(imageViewTarget);

        final Intent intent = getIntent();

        if (intent != null) {
            driveIdVar = intent.getStringExtra("driveId");
            driveLoginTime = intent.getStringExtra("driveLoginTime");
            isDriveLogin = intent.getStringExtra("isDriveLogin");
            messageCount = intent.getStringExtra("messageCount");

            id = intent.getStringExtra("id");
            status = intent.getStringExtra("status");
            name = intent.getStringExtra("name");
            image = intent.getStringExtra("image");
            my_zoe_id = intent.getStringExtra("my_zoe_id");
            responseStr = intent.getStringExtra("responseObj");

            try {
                responseObj = new JSONObject(responseStr);
            } catch (JSONException e) {

            }

            String lastBackUpTime = Utils.getDateFormatted(Long.parseLong(driveLoginTime));
            timeAgo.setText(lastBackUpTime);

        }

        findViewById(R.id.restore).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                signIn();
                getResultsFromApi();
            }
        });

//


        nextScreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.e("checkDownload", "completed:Cancel");

                SharedHelper.putKey(DownloadGoogleDriveFIle.this, "sign", "true");
                SharedHelper.putKey(DownloadGoogleDriveFIle.this, "id", id);
                SharedHelper.putKey(DownloadGoogleDriveFIle.this, "status", status);
                SharedHelper.putKey(DownloadGoogleDriveFIle.this, "name", name);
                SharedHelper.putKey(DownloadGoogleDriveFIle.this, "image", image);
                SharedHelper.putKey(DownloadGoogleDriveFIle.this, "my_zoe_id", my_zoe_id);


                Intent intent = new Intent(DownloadGoogleDriveFIle.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                DownloadGoogleDriveFIle.this.finish();
            }
        });

        mGoogleSignInClient = buildGoogleSignInClient();

        mCredential = GoogleAccountCredential.usingOAuth2(
                getApplicationContext(), Arrays.asList(SCOPES))
                .setBackOff(new ExponentialBackOff());


        findViewById(R.id.skip).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder builder = new AlertDialog.Builder(DownloadGoogleDriveFIle.this, R.style.AlertDialogCustom);

                builder.setTitle("");

                builder.setMessage("Skip restoring your messages? You won\'t be able to restore it later.");

                builder.setPositiveButton(Html.fromHtml("<font color='#00897b'>Skip Restore</font>"), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        skipRestore();
                        dialog.dismiss();
                    }
                });
                builder.setNegativeButton(Html.fromHtml("<font color='#00897b'>Cancel</font>"), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                builder.show();
            }
        });


    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
    private void skipRestore() {

        try {
            JSONObject responseObj = new JSONObject(responseStr);
            JSONArray grpsObj = responseObj.optJSONArray("groups");


            if (grpsObj.length() > 0) {


                doGroupSync = true;
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("zoechatid", SharedHelper.getKey(DownloadGoogleDriveFIle.this, "my_zoe_id"));
                jsonObject.put("accountName", "");
                jsonObject.put("isDriveLogin", "false");
                jsonObject.put("time", "");
                jsonObject.put("driveId", SharedHelper.getKey(DownloadGoogleDriveFIle.this, "googleDriveFileId"));
                jsonObject.put("messageCount", "0");

                new PostHelper(Const.Methods.DRIVE_UPDATE, jsonObject.toString(), Const.ServiceCode.DRIVE_UPDATE_SKIP, DownloadGoogleDriveFIle.this, DownloadGoogleDriveFIle.this);


            } else {
                doGroupSync = false;
                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("zoechatid", SharedHelper.getKey(DownloadGoogleDriveFIle.this, "my_zoe_id"));
                    jsonObject.put("accountName", "");
                    jsonObject.put("isDriveLogin", "false");
                    jsonObject.put("time", "");
                    jsonObject.put("driveId", SharedHelper.getKey(DownloadGoogleDriveFIle.this, "googleDriveFileId"));
                    jsonObject.put("messageCount", "0");

                    new PostHelper(Const.Methods.DRIVE_UPDATE, jsonObject.toString(), Const.ServiceCode.DRIVE_UPDATE_SKIP, DownloadGoogleDriveFIle.this, DownloadGoogleDriveFIle.this);
                } catch (JSONException e) {

                }

            }


        } catch (JSONException e) {

        }


    }

    public void showdialogGroup(String s) {


        dialogGroup = new Dialog(DownloadGoogleDriveFIle.this);
        dialogGroup.setCancelable(false);
        dialogGroup.setCanceledOnTouchOutside(false);

        dialogGroup.setContentView(R.layout.syncing_dialog);
        percentage_text = (TextView) dialogGroup.findViewById(R.id.percentage);
        title = (TextView) dialogGroup.findViewById(R.id.title);


        title.setText("Fetching Messages");

        percentage_text.setVisibility(View.GONE);

        dialogGroup.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialogGroup.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        dialogGroup.show();

    }

    private void getResultsFromApi() {
        if (!isGooglePlayServicesAvailable()) {
            acquireGooglePlayServices();
        } else if (mCredential.getSelectedAccountName() == null) {
            chooseAccount();
        } else if (!isDeviceOnline()) {
            Log.e(TAG, "getResultsFromApi: No network connection available.");
        } else {
            new DownloadGoogleDriveFIle.MakeRequestTask(mCredential).execute();
        }
    }

    public void dismissDialog() {
        if (dialogGroup.isShowing()) {
            dialogGroup.dismiss();


        }
    }

    private void insertGroupMessages(JSONObject responseObj) {


        new getGrpdetails().execute();

    }


    private class getGrpdetails extends AsyncTask<String, Integer, String> {
        String result = "";
        private JSONArray particpants_array = new JSONArray();
        private JSONObject group_details = new JSONObject();

        @Override
        protected void onPostExecute(String s) {
            dismissDialog();

            SharedHelper.putKey(DownloadGoogleDriveFIle.this, "sign", "true");
            SharedHelper.putKey(DownloadGoogleDriveFIle.this, "id", id);
            SharedHelper.putKey(DownloadGoogleDriveFIle.this, "status", status);
            SharedHelper.putKey(DownloadGoogleDriveFIle.this, "name", name);
            SharedHelper.putKey(DownloadGoogleDriveFIle.this, "image", image);
            SharedHelper.putKey(DownloadGoogleDriveFIle.this, "my_zoe_id", my_zoe_id);


            Intent intent = new Intent(DownloadGoogleDriveFIle.this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            DownloadGoogleDriveFIle.this.finish();
        }


        @Override
        protected void onPreExecute() {

            DownloadGoogleDriveFIle.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showdialogGroup("");
                }
            });


        }


        @Override
        protected void onProgressUpdate(Integer... values) {

            super.onProgressUpdate(values);
            int curr_val = values[0];
            int second_val = values[1];

        }

        @Override
        protected String doInBackground(String... params) {


            try {
                jsonObjectWhole = new JSONObject(String.valueOf(responseObj));
                DBHandler dbHandler = new DBHandler(DownloadGoogleDriveFIle.this);

                Log.e("jsonObject", "responseData:" + jsonObjectWhole);

                JSONArray grpsObj = jsonObjectWhole.optJSONArray("groups");

                for (int i = 0; i < grpsObj.length(); i++) {
                    String uniqueID = UUID.randomUUID().toString();

                    JSONObject jsonObject = grpsObj.optJSONObject(i).optJSONObject("_id");

                    Log.e("_idObj", "_idObj:" + jsonObject);

//
                    if (jsonObject.optString("createdBy").equalsIgnoreCase(SharedHelper.getKey(DownloadGoogleDriveFIle.this, "id"))) {
                        String msg = "You created group " + jsonObject.optString("name");
                        dbHandler.InsertChatMsg(new ChatsMessagesModel(uniqueID.trim(), SharedHelper.getKey(DownloadGoogleDriveFIle.this, "id"), msg, ChatType.createGroup.toString(), com.app.fizzychat.models.Status.SENDING.toString(), ChatMessages.CREATE_GROUP, jsonObject.optString("groupId"), "", "0", "0", 0, 0, 0, "0.0", "0.0", "", "", "1", "false", "", "", 0, "", "", "", ""));
                        dbHandler.InsertChats(new ChatsModel(jsonObject.optString("groupId"), "1", ChatMessages.GROUP,
                                msg, com.app.fizzychat.models.Status.SENDING.toString(), jsonObject.optString("createdAt"),
                                "", 0, jsonObject.optString("name"), jsonObject.optString("createdBy"), "", jsonObject.optString("image"), "0", "0", "", "", "", "", "", "", "true", "1", "", ""));


                    } else {
                        String msg = dbHandler.GetUserName(jsonObject.optString("createdBy")) + " created group " + jsonObject.optString("groupName");
                        dbHandler.InsertChatMsg(new ChatsMessagesModel(uniqueID.trim(), SharedHelper.getKey(DownloadGoogleDriveFIle.this, "id"), msg, ChatType.createGroup.toString(), com.app.fizzychat.models.Status.SENDING.toString(), ChatMessages.CREATE_GROUP, jsonObject.optString("groupId"), "", "0", "0", 0, 0, 0, "0.0", "0.0", "", "", "1", "false", "", "", 0, "", "", "", ""));
                        dbHandler.InsertChats(new ChatsModel(jsonObject.optString("groupId"), "1", ChatMessages.GROUP,
                                msg, com.app.fizzychat.models.Status.SENDING.toString(), jsonObject.optString("createdAt"),
                                "", 0, jsonObject.optString("name"), jsonObject.optString("createdBy"), "", jsonObject.optString("image"), "0", "0", "", "", "", "", "", "", "true", "1", "", ""));


                    }
/*
                JSONArray participantsObj = grpsObj.optJSONObject(0).optJSONArray("participants");

                Log.e("participantsObj", "length:" + participantsObj.length());
                for (int j = 0; j < participantsObj.length(); j++) {
                    Log.e("participantsObj", "participantsObj:" + participantsObj);


                    JSONObject jsonObject1 = participantsObj.optJSONObject(j).optJSONObject("participant");
                    Log.e("jsonObject1", "jsonObject1:" + jsonObject1);

                    String id = jsonObject1.optString("_id");
                    String groupId = jsonObject1.optString("groupId");
                    String joinedAt = jsonObject1.optString("joinedAt");
                    String addedBy = jsonObject1.optString("addedBy");
                    String participantId = jsonObject1.optString("participantId");
                    String isAdmin = jsonObject1.optString("isAdmin");

                    Log.e("data", "id:" + id);
                    Log.e("data", "groupId:" + groupId);
                    Log.e("data", "joinedAt:" + joinedAt);
                    Log.e("data", "participantId:" + participantId);
                    Log.e("data", "isAdmin:" + isAdmin);


                }*/
                }


            } catch (JSONException e) {

            }


            JSONObject jsonObject = new JSONObject();
            try {
                DBHandler dbHandler = new DBHandler(DownloadGoogleDriveFIle.this);
                JSONArray grpsObj = jsonObjectWhole.optJSONArray("groups");

                for (int i = 0; i < grpsObj.length(); i++) {
                    JSONArray participantsObj = grpsObj.optJSONObject(0).optJSONArray("participants");

                    for (int j = 0; j < participantsObj.length(); j++) {
                        Log.e("participantsObj", "participantsObj:" + participantsObj);


                        JSONObject jsonObject1 = participantsObj.optJSONObject(j).optJSONObject("participant");
                        Log.e("jsonObject1", "jsonObject1:" + jsonObject1);

                        String id = jsonObject1.optString("_id");
                        String grp_id = jsonObject1.optString("groupId");
                        String joinedAt = jsonObject1.optString("joinedAt");
                        String addedBy = jsonObject1.optString("addedBy");
                        String participantId = jsonObject1.optString("participantId");
                        String isAdmin = jsonObject1.optString("isAdmin");

                        Log.e("data", "id:" + id);
                        Log.e("data", "grp_id:" + grp_id);
                        Log.e("data", "joinedAt:" + joinedAt);
                        Log.e("data", "participantId:" + participantId);
                        Log.e("data", "isAdmin:" + isAdmin);
                        dbHandler.AddGroupParticipants(new GroupParticiapntsModel(id, grp_id, joinedAt, addedBy, isAdmin));

                    }

                }


            } catch (Exception e) {
                e.printStackTrace();
            }
            return result;
        }


    }


    @AfterPermissionGranted(REQUEST_PERMISSION_GET_ACCOUNTS)
    private void chooseAccount() {
        if (EasyPermissions.hasPermissions(
                this, Manifest.permission.GET_ACCOUNTS)) {
            String accountName = getPreferences(Context.MODE_PRIVATE)
                    .getString(PREF_ACCOUNT_NAME, null);
            if (accountName != null) {
                mCredential.setSelectedAccountName(accountName);
                getResultsFromApi();
            } else {
                // Start a dialog from which the user can choose an account
                startActivityForResult(
                        mCredential.newChooseAccountIntent(),
                        REQUEST_ACCOUNT_PICKER);
            }
        } else {
            // Request the GET_ACCOUNTS permission via a user dialog
            EasyPermissions.requestPermissions(
                    this,
                    "This app needs to access your Google account (via Contacts).",
                    REQUEST_PERMISSION_GET_ACCOUNTS,
                    Manifest.permission.GET_ACCOUNTS, Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE);
        }
    }

    @Override
    protected void onPause() {
        if (mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();
        }
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addApi(Drive.API)
                    .addScope(Drive.SCOPE_FILE)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .build();
        }
        mGoogleApiClient.connect();
    }

    private static final String KEY_TEXT_REPLY = "key_text_reply";

    private void inlineReply() {
//        try {
//            // Try to perform a Drive API request, for instance:
        java.io.File folder = Environment.getExternalStorageDirectory();
        String fileName = folder.getPath() + "/Spector/zoechattest.pdf";
        java.io.File myFile = new java.io.File(fileName);

//        File file = service.files().create(myFile).execute();
//        } catch (UserRecoverableAuthIOException e) {
//            startActivityForResult(e.getIntent(), COMPLETE_AUTHORIZATION_REQUEST_CODE);
//        }
//
    }


    private void open() {
//        mProgressBar.setProgress(0);
        DriveFile.DownloadProgressListener listener = new DriveFile.DownloadProgressListener() {
            @Override
            public void onProgress(long bytesDownloaded, long bytesExpected) {
                // Update progress dialog with the latest progress.
                int progress = (int) (bytesDownloaded * 100 / bytesExpected);
                Log.d(TAG, String.format("Loading progress: %d percent", progress));


//                mProgressBar.setProgress(progress);
            }
        };
        DriveFile driveFile = driveId.asDriveFile();
        driveFile.open(mGoogleApiClient, DriveFile.MODE_READ_ONLY, listener)
                .setResultCallback(driveContentsCallback);
        driveId = null;
    }


    private final ResultCallback<DriveApi.DriveContentsResult> driveContentsCallback =
            new ResultCallback<DriveApi.DriveContentsResult>() {
                @Override
                public void onResult(@NonNull DriveApi.DriveContentsResult result) {
                    if (!result.getStatus().isSuccess()) {
                        Log.e(TAG, "Error while opening the file contents");
                        return;
                    }
                    Log.e(TAG, "File contents opened");

                    // Read from the input stream an print to LOGCAT
                    DriveContents driveContents = result.getDriveContents();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(driveContents.getInputStream()));
                    StringBuilder builder = new StringBuilder();
                    String line;
                    try {
                        while ((line = reader.readLine()) != null) {
                            builder.append(line);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    String contentsAsString = builder.toString();
                    Log.e(TAG, contentsAsString);

                    // Close file contents
                    driveContents.discard(mGoogleApiClient);
                }
            };


    private Task<DriveId> pickTextFile() {
        OpenFileActivityOptions openOptions =
                new OpenFileActivityOptions.Builder()
                        .setSelectionFilter(Filters.eq(SearchableField.MIME_TYPE,
                                "text/plain"))
                        .setActivityTitle(getString(R.string.select_file))
                        .build();
        return pickItem(openOptions);
    }


    private Task<DriveId> pickItem(OpenFileActivityOptions openOptions) {
        TaskCompletionSource<DriveId> mOpenItemTaskSource = new TaskCompletionSource<>();
        mDriveClient
                .newOpenFileActivityIntentSender(openOptions)
                .continueWith(new Continuation<IntentSender, Void>() {
                    @Override
                    public Void then(@NonNull Task<IntentSender> task) throws Exception {
                        startIntentSenderForResult(
                                task.getResult(), REQUEST_CODE_OPEN_ITEM, null, 0, 0, 0);
                        return null;
                    }
                });
        return mOpenItemTaskSource.getTask();
    }

    private void listFiles() {
        Query query = new Query.Builder()
                .addFilter(Filters.eq(SearchableField.MIME_TYPE, "text/plain"))
                .build();
        // [START query_files]
        Task<MetadataBuffer> queryTask = mDriveResourceClient.query(query);
        // [END query_files]
        // [START query_results]
        queryTask
                .addOnSuccessListener(this,
                        new OnSuccessListener<MetadataBuffer>() {
                            @Override
                            public void onSuccess(MetadataBuffer metadataBuffer) {
                                // Handle results...
                                // [START_EXCLUDE]
                                Log.e(TAG, "onSuccess: " + metadataBuffer.getCount());
//                                mResultsAdapter.append(metadataBuffer);
                                // [END_EXCLUDE]
                            }
                        })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        // Handle failure...
                        // [START_EXCLUDE]
                        Log.e(TAG, "Error retrieving files", e);
//                        showMessage(getString(R.string.query_failed));
                        finish();
                        // [END_EXCLUDE]
                    }
                });
        // [END query_results]
    }


    private GoogleSignInClient buildGoogleSignInClient() {
        GoogleSignInOptions signInOptions =
                new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                        .requestScopes(Drive.SCOPE_FILE)
                        .build();
        return GoogleSignIn.getClient(this, signInOptions);
    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    protected void signIn() {
        Set<Scope> requiredScopes = new HashSet<>(2);
        requiredScopes.add(Drive.SCOPE_FILE);
        requiredScopes.add(Drive.SCOPE_APPFOLDER);
        GoogleSignInAccount signInAccount = GoogleSignIn.getLastSignedInAccount(this);
        if (signInAccount != null && signInAccount.getGrantedScopes().containsAll(requiredScopes)) {
            initializeDriveClient(signInAccount);
        } else {
            GoogleSignInOptions signInOptions =
                    new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                            .requestScopes(Drive.SCOPE_FILE)
                            .requestScopes(Drive.SCOPE_APPFOLDER)
                            .build();
            GoogleSignInClient googleSignInClient = GoogleSignIn.getClient(this, signInOptions);
            startActivityForResult(googleSignInClient.getSignInIntent(), REQUEST_CODE_CAPTURE_IMAGE);
        }
    }


    private void initializeDriveClient(GoogleSignInAccount signInAccount) {
        mDriveClient = Drive.getDriveClient(getApplicationContext(), signInAccount);
        mDriveResourceClient = Drive.getDriveResourceClient(getApplicationContext(), signInAccount);
//        onDriveClientReady();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(
                requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> list) {
        // Do nothing.
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> list) {
        // Do nothing.
    }

    private boolean isDeviceOnline() {
        ConnectivityManager connMgr =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }

    private boolean isGooglePlayServicesAvailable() {
        GoogleApiAvailability apiAvailability =
                GoogleApiAvailability.getInstance();
        final int connectionStatusCode =
                apiAvailability.isGooglePlayServicesAvailable(this);
        return connectionStatusCode == ConnectionResult.SUCCESS;
    }

    private void acquireGooglePlayServices() {
        GoogleApiAvailability apiAvailability =
                GoogleApiAvailability.getInstance();
        final int connectionStatusCode =
                apiAvailability.isGooglePlayServicesAvailable(this);
        if (apiAvailability.isUserResolvableError(connectionStatusCode)) {
            showGooglePlayServicesAvailabilityErrorDialog(connectionStatusCode);
        }
    }

    void showGooglePlayServicesAvailabilityErrorDialog(
            final int connectionStatusCode) {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        Dialog dialog = apiAvailability.getErrorDialog(
                DownloadGoogleDriveFIle.this,
                connectionStatusCode,
                REQUEST_GOOGLE_PLAY_SERVICES);
        dialog.show();
    }

    private class MakeRequestTask extends AsyncTask<Void, Void, List<String>> {
        com.google.api.services.drive.Drive mService = null;
        private Exception mLastError = null;
        GoogleAccountCredential credential;

        MakeRequestTask(GoogleAccountCredential credential) {
            this.credential = credential;
            HttpTransport transport = AndroidHttp.newCompatibleTransport();
            JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();
            mService = new com.google.api.services.drive.Drive.Builder(
                    transport, jsonFactory, credential)
                    .setApplicationName("Zoi Files")
                    .build();

        }

        private File uploadFile(boolean useDirectUpload) throws IOException {

            service = new com.google.api.services.drive.Drive.Builder(AndroidHttp.newCompatibleTransport(), new GsonFactory(),
                    credential).setApplicationName("ZoeFile").build();

            File fileMetadata = new File();
            fileMetadata.setName("Zeochat");
            fileMetadata.setMimeType("application/x-sqlite3");
            fileMetadata.setDescription("This is a sample pdf file");
            fileMetadata.setParents(Collections.singletonList("appDataFolder"));


            java.io.File direct = new java.io.File(Environment.getExternalStorageDirectory()
                    + "/Spector");
            if (!direct.exists()) {
                direct.mkdirs();
            }

            java.io.File folder = Environment.getExternalStorageDirectory();
            // String fileName = folder.getPath() + "/Spector/zoechattest.pdf";
            String currentDBPath = "//data//" + "//data//" + getPackageName()
                    + "//databases//" + DBHandler.DATABASE_NAME;

            Log.e(TAG, "currentDBPath:" + currentDBPath);

            java.io.File myFile = new java.io.File(currentDBPath);


//        FileList files = service.files().list().setQ("title = 'Zoechat' and trashed = false").execute();
//        Log.e("TestingActivity", "uploadFile: " + files.getItems().size());
//            com.google.api.services.drive.model.File directory = mService.files().create(fileMetadata)
//                    .execute();
//            Log.e("TestingActivity", "uploadFile: " + directory.getId() + directory);
//        if (myFile.exists())
//            myFile.delete();

            FileContent mediaContent = new FileContent("application/x-sqlite3", myFile);

            com.google.api.services.drive.Drive.Files.Create insert = service.files().create
                    (fileMetadata,
                            mediaContent);
            MediaHttpUploader uploader = insert.getMediaHttpUploader();
            uploader.setDirectUploadEnabled(false);
            uploader.setChunkSize(MediaHttpUploader.MINIMUM_CHUNK_SIZE);
            //uploader.setProgressListener(new FileUploadProgressListener());

            uploader.setProgressListener(new MediaHttpUploaderProgressListener() {
                @Override
                public void progressChanged(MediaHttpUploader uploader) throws IOException {
                    Log.e(TAG, "progressValue:" + uploader.getProgress());

                    if (uploader.getProgress() == 0.0) {
                        pbProcessing.setProgress(50);

                    } else if (uploader.getProgress() == 1.0) {

                        pbProcessing.setProgress(100);

                    }

                }
            });


//            uploader.setProgressListener(new FileUploadProgressListener());
            return insert.execute();
        }


        public class FileUploadProgressListener implements MediaHttpUploaderProgressListener {

            public void progressChanged(MediaHttpUploader uploader) throws IOException {
                double i = uploader.getProgress();
                Log.e(TAG, "valuesis:" + i);
                if (i == 0.0) {// postToDialog("Initiation Started");
                    //downloadStatus.setText("Upload Started");

                    pbProcessing.setProgress(50);
//                    downloadStatus.setText("50%");
                    Log.e(TAG, "progressChanged:" + "0to20");
                } else if (i == 1.0) {// postToDialog("Initiation Completed");
                    pbProcessing.setMax(100);
                    pbProcessing.setProgress(100);
                    //downloadStatus.setText("100%");
                    pbProcessing.setVisibility(View.GONE);
                    Log.e(TAG, "progressChanged:" + "20to40");
                }
            }
        }

        private void downloadFile(boolean useDirectDownload, File uploadedFile)
                throws IOException {

//            service = new com.google.api.services.drive.Drive.Builder(AndroidHttp.newCompatibleTransport(), new GsonFactory(),
//                    credential).setApplicationName("ZoeFile").build();


            java.io.File direct = new java.io.File(Environment.getExternalStorageDirectory()
                    + "/Spectors");


//            String currentDBPath = "//data//"+getPackageName()
//                    + "//databases//" + DBHandler.DATABASE_NAME;

            //String currentDBPath=  Environment.getExternalStorageDirectory() + "/Zoechat/";

            String currentDBPath = "//data//" + getPackageName()
                    + "//databases//" + DBHandler.DATABASE_NAME;

            java.io.File data = Environment.getDataDirectory();


            java.io.File currentDB = new java.io.File(data, currentDBPath);

            if (!direct.exists()) {
                direct.mkdirs();
            }

            java.io.File parentDir = new java.io.File(currentDBPath);
            if (!currentDB.exists() && !currentDB.mkdirs()) {
                //  throw new IOException("Unable to create parent directory");
                currentDB.createNewFile();
            }


            Log.e(TAG, "currentDB:" + currentDB);
            OutputStream out = new FileOutputStream(currentDB);


            com.google.api.services.drive.Drive.Files.Get get = mService.files().get(driveIdVar);
            get.getMediaHttpDownloader().setProgressListener(new DownloadProgressListener());
            get.executeMediaAndDownloadTo(out);


            com.google.api.services.drive.Drive.Files.Export files = mService.files().export
                    (driveIdVar,
                            uploadedFile.getMimeType() == null ? "application/x-sqlite3" : uploadedFile.getMimeType());


            Log.e(TAG, "downloadFile: " + files.toString() + "  " + files.getLastResponseHeaders() +
                    "\n" + files.getDisableGZipContent() + "\n" + files.getFields());

            MediaHttpDownloader downloader =
                    new MediaHttpDownloader(new NetHttpTransport(), mService.getRequestFactory()
                            .getInitializer());
            downloader.setDirectDownloadEnabled(false);

            downloader.setProgressListener(new MediaHttpDownloaderProgressListener() {
                @Override
                public void progressChanged(MediaHttpDownloader downloader) throws IOException {
                    Log.e(TAG, "downProgress:" + downloader.getProgress());
                }
            });


            Log.e(TAG, "downloadFile: " + out + "\n" + uploadedFile
                    .getThumbnailLink()
                    + "\n" + mService.files().export(driveIdVar, uploadedFile
                    .getMimeType()) +
                    "\n" +
                    downloader.getTransport().createRequestFactory().getInitializer());
        }


        /**
         * Background task to call Drive API.
         *
         * @param params no parameters needed for this task.
         */
        @Override
        protected List<String> doInBackground(Void... params) {
            try {
                // File file = uploadFile(false);
//                downloadFile(true, file);


                DownloadGoogleDriveFIle.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        restoreContent.setVisibility(View.GONE);
                        progressGifLinear.setVisibility(View.VISIBLE);
                        restoreLinear.setVisibility(View.GONE);
                    }
                });

                return getDataFromApi();
            } catch (Exception e) {
                mLastError = e;
                cancel(true);
                return null;
            }
        }

        /**
         * Fetch a list of up to 10 file names and IDs.
         *
         * @return List of Strings describing files, or an empty list if no files
         * found.
         * @throws IOException
         */
        private List<String> getDataFromApi() throws IOException {
            // Get a list of up to 10 files.


            DownloadGoogleDriveFIle.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    restoringText.setText("Restoring messages (40%)");
                }
            });


            List<String> fileInfo = new ArrayList<String>();
            FileList lst = mService.files().list().setSpaces("appDataFolder").execute();
            List<File> fs = lst.getFiles();
            if (lst != null) {


                for (File fil : fs) {
                    Log.e(TAG, "listOfFilesAre: " + fil + "\n" + fil.getId().getBytes() + "\n" + fil.getTeamDriveId());
                    // Log.e(TAG, "listOfFilesAre: " + fil + "\n" + fil.getWebViewLink() + "\n" + fil.getSize());
                    downloadFile(false, fil);
                }
            }
            FileList result = mService.files().list()
                    .setSpaces("appDataFolder")
                    .setFields("nextPageToken, files(id, name, mimeType)")
                    .execute();
            List<File> files = result.getFiles();
            if (files != null) {
                for (File file : files) {
                    fileInfo.add(String.format("%s (%s)\n",
                            file.getName(), file.getId()));
                }
            }
            return fileInfo;
        }


        @Override
        protected void onPreExecute() {
//            mOutputText.setText("");
//            mProgress.show();
        }

        @Override
        protected void onPostExecute(List<String> output) {
//            mProgress.hide();
            if (output == null || output.size() == 0) {
//                mOutputText.setText("No results returned.");
                Log.e(TAG, "onPostExecute: No Result Returned");
            } else {
                output.add(0, "Data retrieved using the Drive API:");
                Log.e(TAG, "onPostExecute: " + TextUtils.join("\n", output));
//                mOutputText.setText(TextUtils.join("\n", output));
            }
        }

        @Override
        protected void onCancelled() {
//            mProgress.hide();
            if (mLastError != null) {
                if (mLastError instanceof GooglePlayServicesAvailabilityIOException) {
                    showGooglePlayServicesAvailabilityErrorDialog(
                            ((GooglePlayServicesAvailabilityIOException) mLastError)
                                    .getConnectionStatusCode());
                } else if (mLastError instanceof UserRecoverableAuthIOException) {
                    startActivityForResult(
                            ((UserRecoverableAuthIOException) mLastError).getIntent(),
                            DownloadGoogleDriveFIle.REQUEST_AUTHORIZATION);
                } else {
                    Log.e(TAG, "onCancelled: ", mLastError);
                    Log.e(TAG, "onCancelled: " + mLastError.getMessage());
//                    mOutputText.setText("The following error occurred:\n"
//                            + mLastError.getMessage());
                }
            } else {
                Log.e(TAG, "onCancelled: Cancel");
//                mOutputText.setText("Request cancelled.");
            }
        }
    }


    //custom listener for download progress
    class DownloadProgressListener implements MediaHttpDownloaderProgressListener {


        @Override
        public void progressChanged(MediaHttpDownloader downloader) throws IOException {

            Log.e("downloader", "dxs" + downloader.getProgress());

            switch (downloader.getDownloadState()) {

                //Called when file is still downloading
                //ONLY CALLED AFTER A CHUNK HAS DOWNLOADED,SO SET APPROPRIATE CHUNK SIZE
                case MEDIA_IN_PROGRESS:
                    //Add code for showing progress
                    Log.e("checkDownload", "InProgress:Cancel");

                    DownloadGoogleDriveFIle.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            restoringText.setText("Restoring messages(70%)");
                        }
                    });

                    break;
                //Called after download is complete
                case MEDIA_COMPLETE:
                    //Add code for download completion
                    //restoringText.setText("Restoring messages (90%)");
                    DownloadGoogleDriveFIle.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            restoringText.setText("Restoring messages(90%)");
                            JSONObject jsonObject = new JSONObject();
                            try {
                                jsonObject.put("zoechatid", SharedHelper.getKey(DownloadGoogleDriveFIle.this, "my_zoe_id"));
                                jsonObject.put("accountName", "");
                                jsonObject.put("isDriveLogin", "false");
                                jsonObject.put("time", "");
                                jsonObject.put("driveId", SharedHelper.getKey(DownloadGoogleDriveFIle.this, "googleDriveFileId"));
                                jsonObject.put("messageCount", "0");


                            } catch (JSONException e) {

                            }

                            Log.e("getting_ph", jsonObject.toString());
                            new PostHelper(Const.Methods.DRIVE_UPDATE, jsonObject.toString(), Const.ServiceCode.DRIVE_UPDATE, DownloadGoogleDriveFIle.this, DownloadGoogleDriveFIle.this);


                        }
                    });


                    break;
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_CODE_CAPTURE_IMAGE:
                if (resultCode != RESULT_OK) {
                    // Sign-in may fail or be cancelled by the user. For this sample, sign-in is
                    // required and is fatal. For apps where sign-in is optional, handle
                    // appropriately
                    Log.e(TAG, "Sign-in failed.");
                    finish();
                    return;
                }

                Task<GoogleSignInAccount> getAccountTask =
                        GoogleSignIn.getSignedInAccountFromIntent(data);
                if (getAccountTask.isSuccessful()) {
                    initializeDriveClient(getAccountTask.getResult());
                } else {
                    Log.e(TAG, "Sign-in failed.");
                    finish();
                }
                break;
            case REQUEST_CODE_OPEN_ITEM:
                if (resultCode == RESULT_OK) {
                    driveId = data.getParcelableExtra(
                            OpenFileActivityOptions.EXTRA_RESPONSE_DRIVE_ID);
                    Log.e(TAG, "onActivityResult: " + driveId);

                    open();


//                    mOpenItemTaskSource.setResult(driveId);
                } else {
                    Log.e(TAG, "onActivityResult: unable to open ");
                }
                break;

            case COMPLETE_AUTHORIZATION_REQUEST_CODE:
                if (resultCode == Activity.RESULT_OK) {
                    Log.e(TAG, "onActivityResult: auth");
                    // App is authorized, you can go back to sending the API request
//                    inlineReply();
                } else {
                    // User denied access, show him the account chooser again
                    Log.e(TAG, "onActivityResult: denied");
                }
                break;

            case REQUEST_GOOGLE_PLAY_SERVICES:
                if (resultCode != RESULT_OK) {
//                    mOutputText.setText(
//                            "This app requires Google Play Services. Please install " +
//                                    "Google Play Services on your device and relaunch this app.");
                    Log.e(TAG, "onActivityResult: require google play");
                } else {
                    getResultsFromApi();
                }
                break;
            case REQUEST_ACCOUNT_PICKER:
                if (resultCode == RESULT_OK && data != null &&
                        data.getExtras() != null) {
                    String accountName =
                            data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
                    if (accountName != null) {
                        DBHandler dbHandler = new DBHandler(DownloadGoogleDriveFIle.this);
                        dbHandler.CreateTables();
                        SharedPreferences settings =
                                getPreferences(Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = settings.edit();
                        editor.putString(PREF_ACCOUNT_NAME, accountName);
                        editor.apply();
                        mCredential.setSelectedAccountName(accountName);
                        getResultsFromApi();
                    }
                }
                break;
            case REQUEST_AUTHORIZATION:
                if (resultCode == RESULT_OK) {
                    getResultsFromApi();
                }
                break;

        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onTaskCompleted(JSONObject response, int serviceCode) {
        switch (serviceCode) {

            case Const.ServiceCode.DRIVE_UPDATE:
                Log.e("response", "" + response);
                if (response != null) {
                    if (response.optString("error").equalsIgnoreCase("false")) {

                        SharedHelper.putKey(DownloadGoogleDriveFIle.this, "sign", "true");
                        SharedHelper.putKey(DownloadGoogleDriveFIle.this, "id", id);
                        SharedHelper.putKey(DownloadGoogleDriveFIle.this, "status", status);
                        SharedHelper.putKey(DownloadGoogleDriveFIle.this, "name", name);
                        SharedHelper.putKey(DownloadGoogleDriveFIle.this, "image", image);
                        SharedHelper.putKey(DownloadGoogleDriveFIle.this, "my_zoe_id", my_zoe_id);

                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                restoringText.setText("Restoring messages (100%)");

                                restoredMessages.setText(messageCount + " messages restored");
                                progressGifLinear.setVisibility(View.GONE);
                                doneLinear.setVisibility(View.VISIBLE);

                            }
                        }, 300);

                    } else {

                    }
                } else {
                    Utils.showLongToast(getResources().getString(R.string.server_error), getApplicationContext());
                }

                break;


            case Const.ServiceCode.DRIVE_UPDATE_SKIP:


                DBHandler dbHandler = new DBHandler(DownloadGoogleDriveFIle.this);
                dbHandler.CreateTables();
                new readAllContacts().execute();


                break;
        }

    }

    private class readAllContacts extends AsyncTask<String, Integer, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showdialog();

        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                DBHandler dbHandler = new DBHandler(DownloadGoogleDriveFIle.this);
                ContentResolver cr = getContentResolver();
                Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,
                        null, null, null, null);


                if (cur.getCount() > 0) {
                    while (cur.moveToNext()) {
                        String id = cur.getString(
                                cur.getColumnIndex(ContactsContract.Contacts._ID));
                        String name = cur.getString(cur.getColumnIndex(
                                ContactsContract.Contacts.DISPLAY_NAME));

                        if (cur.getInt(cur.getColumnIndex(
                                ContactsContract.Contacts.HAS_PHONE_NUMBER)) > 0) {

                            publishProgress(cur.getPosition(), cur.getCount());

                            Cursor pCur = cr.query(
                                    ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                                    null,
                                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                                    new String[]{id}, null);

                            while (pCur.moveToNext()) {
                                String phoneNo = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                                String log = "Name: " + name + ", Phone No: " + phoneNo;
                                //Log.e("log", "" + log);
                                String rp_phone;
                                if (phoneNo.contains("+")) {
                                    rp_phone = phoneNo.replaceAll("[\\-\\+\\.\\^:,\\s+\\(\\)\\#\\*]", "");
                                } else {
                                    String number = SharedHelper.getKey(DownloadGoogleDriveFIle.this, "country_code_default");
                                    rp_phone = number + phoneNo;
                                    rp_phone = rp_phone.replaceAll("[\\-\\+\\.\\^:,\\s+\\(\\)\\#\\*]", "");

                                }

                                if (rp_phone.length() < 14) {
                                    if (rp_phone.length() > 3) {
                                        try {

                                            String registerNumber = SharedHelper.getKey(DownloadGoogleDriveFIle.this, "my_zoe_id");
                                            registerNumber = registerNumber.substring(registerNumber.length() - 10);
                                            if (!rp_phone.equalsIgnoreCase(registerNumber)) {
                                                if (!dbHandler.CheckIsDataAlreadyInDBorNot(rp_phone)) {
                                                    dbHandler.InsertUser(new ContactsModel(rp_phone, name, "", "", "", 0, "", id, false));
                                                } else {
                                                    dbHandler.UserNameUpdate(name, rp_phone);
                                                }
                                            }
                                        } catch (NullPointerException e) {

                                        }
                                    }
                                }

                            }
                            pCur.close();
                        }
                    }
                }
                cur.close();
                dbHandler.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            int curr_val = values[0];
            int second_val = values[1];

            setpercentage(curr_val, second_val, 1);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            new getContacts().execute();

        }

    }


    private void setpercentage(int curr_val, int second_val, int last_per) {
        Long percentage = (long) ((float) curr_val / second_val * 100);
        Long initpercentage = (long) ((float) percentage / 100 * 30);
        final_percentage = last_per + initpercentage;
        try {
            if (dialog.isShowing()) {
                percentage_text.setText("" + final_percentage + "%");
            }
        } catch (Exception e) {

        }
    }


    public void showdialog() {
        dialog = new Dialog(DownloadGoogleDriveFIle.this);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);

        dialog.setContentView(R.layout.syncing_dialog);
        percentage_text = (TextView) dialog.findViewById(R.id.percentage);
        percentage_text.setText("" + final_percentage + "%");

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        dialog.show();

    }


    public void dismiss() {
        if (dialog.isShowing()) {
            dialog.dismiss();
            movetomain();

        }
    }

    private void movetomain() {

        if (doGroupSync) {
            insertGroupMessages(responseObj);
            doGroupSync = false;
        } else {
            Intent intent = new Intent(DownloadGoogleDriveFIle.this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            DownloadGoogleDriveFIle.this.finish();
        }

    }

    private class getContacts extends AsyncTask<String, Integer, String> implements AsyncTaskCompleteListener {
        String result = "";

        @Override
        protected String doInBackground(String... strings) {
            JSONArray jsonArray = new JSONArray();
            DBHandler dbHandler = new DBHandler(DownloadGoogleDriveFIle.this);
            try {


                contact_list = dbHandler.GetAllUserFromDB();

//                Log.d(TAG, "doInBackground: " + contact_list);


                //Utils.appLog("jsonArray", contact_list.toString());

                for (int i = 0; i < contact_list.size(); i++) {
                    publishProgress(i + 1, contact_list.size());
                    JSONObject jsonObject = new JSONObject();
                    try {
                        if (!contact_list.get(i).getMobile().equalsIgnoreCase(SharedHelper.getKey(DownloadGoogleDriveFIle.this, "my_zoe_id"))) {
                            jsonObject.put("mobileNumber", contact_list.get(i).getMobile());
                            jsonObject.put("contactName", contact_list.get(i).getName());
//                            Log.d(TAG, "doInBackground: "+jsonObject);
                            jsonArray.put(jsonObject);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                Collections.sort(contact_list, new Comparator<GetUserFromDBModel>() {
                    @Override
                    public int compare(GetUserFromDBModel getUserFromDB, GetUserFromDBModel t1) {
                        return getUserFromDB.getName().toLowerCase().compareTo(t1.getName().toLowerCase());
                    }
                });
            } catch (NullPointerException e) {
                e.printStackTrace();
            }

            //Utils.appLog("get_contact_FromDB", jsonArray.toString());
            JSONObject jsonObject = new JSONObject();

            try {
                jsonObject.put("contacts", jsonArray.toString());
                jsonObject.put("from", SharedHelper.getKey(DownloadGoogleDriveFIle.this, "my_zoe_id"));
                Log.d("doInBackground: ", "input:" + jsonObject);
//                Utils.appLog("Contact_log", jsonArray.toString());
//                if (internet) {
                new PostHelper(Const.Methods.SYNC_CONTACTS, jsonObject.toString(), Const.ServiceCode.SYNC_CONTACTS, DownloadGoogleDriveFIle.this, this);


            } catch (JSONException e) {
                e.printStackTrace();
            }
            return result;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            int curr_val = values[0];
            int second_val = values[1];

            setpercentage(curr_val, second_val, 31);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

        }

        @Override
        public void onTaskCompleted(final JSONObject response, int serviceCode) {
            switch (serviceCode) {
                case Const.ServiceCode.SYNC_CONTACTS:
//                    Utils.appLog("Contact_Response", response.toString());
                    if (response != null) {
                        if (response.optString("error").equalsIgnoreCase("false")) {
                            JSONArray jsonArray = response.optJSONArray("contacts");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                setpercentage(i + 1, jsonArray.length(), 61);
                                JSONObject jsonObject = jsonArray.optJSONObject(i);
                                String mobile_id = jsonObject.optString("mobileNumber");
                                DBHandler dbHandler = new DBHandler(DownloadGoogleDriveFIle.this);
                                try {
                                    if (!dbHandler.CheckIsDataAlreadyInDBorNot(mobile_id)) {
                                        if (jsonObject.optString("showInContactsPage").equalsIgnoreCase("true")) {
                                            dbHandler.InsertUser(new ContactsModel(mobile_id, jsonObject.optString("name"), jsonObject.optString("image"), "", jsonObject.optString("status"), 1, "", jsonObject.optString("contactid"), false));
                                        } else {
                                            if (dbHandler.UserUpdate(mobile_id, jsonObject.optString("name"), jsonObject.optString("image"), "1", jsonObject.optString("zoechatid"), jsonObject.optString("status")) > 0) {
                                            } else {
                                                dbHandler.InsertUser(new ContactsModel(mobile_id, jsonObject.optString("name"), "", "", jsonObject.optString("status"), 0, "", jsonObject.optString("contactid"), false));
                                            }
                                        }

                                    } else if (jsonObject.optString("showInContactsPage").equalsIgnoreCase("true")) {
                                        dbHandler.UserUpdate(mobile_id, jsonObject.optString("name"), jsonObject.optString("image"), "1", jsonObject.optString("zoechatid"), jsonObject.optString("status"));
                                    }
                                    dbHandler.close();
                                } catch (NullPointerException e) {
                                    e.printStackTrace();
                                }
                            }

                            dismiss();


                        } else {
                            try {

                                DownloadGoogleDriveFIle.this.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(DownloadGoogleDriveFIle.this, response.optString("message"), Toast.LENGTH_SHORT).show();
                                    }
                                });
                            } catch (NullPointerException e) {

                            }
                            //new printListOfContacts().execute();
                        }
                    } else {
                        try {
                            DownloadGoogleDriveFIle.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(DownloadGoogleDriveFIle.this, response.optString("message"), Toast.LENGTH_SHORT).show();
                                }
                            });
                        } catch (NullPointerException e) {

                        }

                    }

                    break;
            }

        }
    }

}

