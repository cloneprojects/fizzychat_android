package com.app.fizzychat.activity;

import android.app.Activity;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;


import com.vanniktech.emoji.EmojiEditText;
import com.vanniktech.emoji.EmojiPopup;
import com.vanniktech.emoji.emoji.Emoji;
import com.vanniktech.emoji.listeners.OnEmojiBackspaceClickListener;
import com.vanniktech.emoji.listeners.OnEmojiClickedListener;
import com.vanniktech.emoji.listeners.OnEmojiPopupDismissListener;
import com.vanniktech.emoji.listeners.OnEmojiPopupShownListener;
import com.vanniktech.emoji.listeners.OnSoftKeyboardCloseListener;
import com.vanniktech.emoji.listeners.OnSoftKeyboardOpenListener;
import com.app.fizzychat.DBHelper.DBHandler;
import com.app.fizzychat.R;
import com.app.fizzychat.Service.ServiceClasss;
import com.app.fizzychat.baseUtils.SharedHelper;
import com.app.fizzychat.baseUtils.Utils;
import com.app.fizzychat.fragment.ChatFragment;
import com.app.fizzychat.models.ChatType;
import com.app.fizzychat.models.ScheduleTextModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.UUID;


import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static android.view.View.GONE;

public class ScheduleChatActivity extends AppCompatActivity {
    private static final String TAG = ScheduleChatActivity.class.getName();
    private EmojiEditText chat_text;
    private TextView date_details, time_details, contacts_details, selected_time;
    private LinearLayout select_time_linear, plus_layout, closeLayout;
    public static JSONArray jsonObject = null;
    private String selectedTime, userSelectedTime = "";
    private CardView card_visibility;
    private EmojiPopup emojiPopup;
    private ViewGroup rootView;
    private ImageView plusImage, plusIcon, hideImage;
    FrameLayout bottomlayout2;
    RelativeLayout smileyLayout, backPressed;
    private int mYear, mMonth, mDay, mHour, mMinute, houOfDay24Format, minute24Format;
    private ImageButton send_chat;
    public static List<JSONObject> shcedule_text_list;
    private boolean dateSelected = false;
    private boolean timeSelected = false;
    private boolean contcatSelected = false;
    ArrayList<String> days;
    private Toolbar schedule_chat_toolbar;
    TextView day1, day2, day3, day4, day5, day6, day7;
    String userSelectedDate = "";
    FrameLayout send_chat_layout;
    DBHandler dbHandler;
    String myDate;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void Setheme(String themevalue) {
        try {
            switch (themevalue) {
                case "1":
                    setTheme(R.style.AppThemeGreen);
                    setGradientColor(1);
                    break;
                case "2":
                    setTheme(R.style.AppThemeBlue);
                    setGradientColor(2);
                    break;
                case "3":
                    setTheme(R.style.AppThemeIndigo);
                    setGradientColor(3);
                    break;
                case "4":
                    setTheme(R.style.AppThemeGrey);
                    setGradientColor(4);
                    break;
                case "5":
                    setTheme(R.style.AppThemeYellow);
                    setGradientColor(5);
                    break;
                case "6":
                    setTheme(R.style.AppThemeOrange);
                    setGradientColor(6);
                    break;
                case "7":
                    setTheme(R.style.AppThemePurple);
                    setGradientColor(7);
                    break;
                case "8":
                    setTheme(R.style.AppThemePaleGreen);
                    setGradientColor(8);
                    break;
                case "9":
                    setTheme(R.style.AppThemelightBlue);
                    setGradientColor(9);
                    break;
                case "10":
                    setTheme(R.style.AppThemePink);
                    setGradientColor(10);
                    break;
                case "11":
                    setTheme(R.style.AppThemelightGreen);
                    setGradientColor(11);
                    break;
                case "12":
                    setTheme(R.style.AppThemelightRed);
                    setGradientColor(12);
                    break;
                default:
                    setTheme(R.style.AppThemeGreen);
                    setGradientColor(1);
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setGradientColor(int i) {

        try {
            schedule_chat_toolbar.setBackgroundColor(getPrimaryCOlor(ScheduleChatActivity.this));
            send_chat.setBackgroundColor(getPrimaryCOlor(ScheduleChatActivity.this));
//            if (i == 1) {
//                schedule_chat_toolbar.setBackground(getResources().getDrawable(R.drawable.gradient_colour));
//                send_chat.setBackground(getPrimaryCOlor(ScheduleChatActivity.this));
//                //day1.setBackground(getResources().getDrawable(R.drawable.gradient_color_circle));
//            } else if (i == 2) {
//                schedule_chat_toolbar.setBackground(getResources().getDrawable(R.drawable.gradient_color_teo));
//                send_chat.setBackground(getResources().getDrawable(R.drawable.gradient_color_teo));
//                //day1.setBackground(getResources().getDrawable(R.drawable.gradient_color_circle_2));
//            } else if (i == 3) {
//                schedule_chat_toolbar.setBackground(getResources().getDrawable(R.drawable.gradient_color_three));
//                send_chat.setBackground(getResources().getDrawable(R.drawable.gradient_color_three));
//                //day1.setBackground(getResources().getDrawable(R.drawable.gradient_circle_3));
//            } else if (i == 4) {
//                schedule_chat_toolbar.setBackground(getResources().getDrawable(R.drawable.gradient_color_four));
//                send_chat.setBackground(getResources().getDrawable(R.drawable.gradient_color_four));
//                // day1.setBackground(getResources().getDrawable(R.drawable.gradient_circle_4));
//            } else if (i == 5) {
//                schedule_chat_toolbar.setBackground(getResources().getDrawable(R.drawable.gradient_color_five));
//                send_chat.setBackground(getResources().getDrawable(R.drawable.gradient_color_five));
//                // day1.setBackground(getResources().getDrawable(R.drawable.gradient_circle_5));
//            } else if (i == 6) {
//                schedule_chat_toolbar.setBackground(getResources().getDrawable(R.drawable.gradient_color_six));
//                send_chat.setBackground(getResources().getDrawable(R.drawable.gradient_color_six));
//                //day1.setBackground(getResources().getDrawable(R.drawable.gradient_circle_6));
//            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void setDateBackground1(int i) {


        try {
            day1.setBackgroundColor(getPrimaryCOlor(ScheduleChatActivity.this));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setDateBackground2(int i) {

        try {
            day2.setBackgroundColor(getPrimaryCOlor(ScheduleChatActivity.this));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void setDateBackground3(int i) {

//

        try {
            day3.setBackgroundColor(getPrimaryCOlor(ScheduleChatActivity.this));

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void setDateBackground4(int i) {

//
        try {
            day4.setBackgroundColor(getPrimaryCOlor(ScheduleChatActivity.this));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void setDateBackground5(int i) {

//
        try {
            day5.setBackgroundColor(getPrimaryCOlor(ScheduleChatActivity.this));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void setDateBackground6(int i) {

        try {
            day6.setBackgroundColor(getPrimaryCOlor(ScheduleChatActivity.this));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void setDateBackground7(int i) {

        try {
            day7.setBackgroundColor(getPrimaryCOlor(ScheduleChatActivity.this));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static int getPrimaryCOlor(Context context) {
        final TypedValue value = new TypedValue();
        context.getTheme().resolveAttribute(R.attr.colorPrimary, value, true);
        return value.data;
    }

    public void setIconColour(Drawable drawable, int primaryCOlor) {

        drawable.setColorFilter(new PorterDuffColorFilter(getPrimaryCOlor(ScheduleChatActivity.this), PorterDuff.Mode.SRC_IN));

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final String themevalue = SharedHelper.getKey(this, "theme_value");
        Setheme(themevalue);
        setContentView(R.layout.activity_schedule_chat);

        chat_text = (EmojiEditText) findViewById(R.id.edittext);
        time_details = (TextView) findViewById(R.id.time_details);
        schedule_chat_toolbar = (Toolbar) findViewById(R.id.schedule_chat_toolbar);
        date_details = (TextView) findViewById(R.id.date_details);
        contacts_details = (TextView) findViewById(R.id.contacts_details);
        selected_time = (TextView) findViewById(R.id.selected_time);
        card_visibility = (CardView) findViewById(R.id.card_visibility);
        plusImage = (ImageView) findViewById(R.id.plusImage);
        plusIcon = (ImageView) findViewById(R.id.plusIcon);
        //hideImage = (ImageView) findViewById(R.id.hideImage);
        backPressed = (RelativeLayout) findViewById(R.id.backPressed);
        smileyLayout = (RelativeLayout) findViewById(R.id.smileyLayout);
        send_chat = (ImageButton) findViewById(R.id.send_chat);
        chat_text = (EmojiEditText) findViewById(R.id.edittext);
        send_chat_layout = (FrameLayout) findViewById(R.id.send_chat_layout);
        days = new ArrayList<>();
        day1 = (TextView) findViewById(R.id.day1);
        day2 = (TextView) findViewById(R.id.day2);
        day3 = (TextView) findViewById(R.id.day3);
        day4 = (TextView) findViewById(R.id.day4);
        day5 = (TextView) findViewById(R.id.day5);
        day6 = (TextView) findViewById(R.id.day6);
        day7 = (TextView) findViewById(R.id.day7);
        rootView = (ViewGroup) findViewById(R.id.schedule_chat);
        dbHandler = new DBHandler(ScheduleChatActivity.this);
        Setheme(themevalue);
        setUpEmojiPopup();

        setIconColour(plusIcon.getDrawable(), getPrimaryCOlor(ScheduleChatActivity.this));
        setIconColour(plusImage.getDrawable(), getPrimaryCOlor(ScheduleChatActivity.this));

        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
        String currentDateandTime = sdf.format(new Date());
        shcedule_text_list = new ArrayList<>();
        final JSONObject message_data_obj = new JSONObject();

        SimpleDateFormat sdff = new SimpleDateFormat("dd-MM-yyyy");
        for (int i = 0; i < 7; i++) {
            Calendar calendar = new GregorianCalendar();
            calendar.add(Calendar.DATE, i);
            String day = sdff.format(calendar.getTime());
            Log.e("Schedule", day);
            days.add(day);
        }

        selected_time.setText(currentDateandTime);

        date_details.setText(days.get(0) + ",");
        userSelectedDate = days.get(0);

        day1.setText(days.get(0).substring(0, 2));
        day2.setText(days.get(1).substring(0, 2));
        day3.setText(days.get(2).substring(0, 2));
        day4.setText(days.get(3).substring(0, 2));
        day5.setText(days.get(4).substring(0, 2));
        day6.setText(days.get(5).substring(0, 2));
        day7.setText(days.get(6).substring(0, 2));


        backPressed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        send_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {

                    if (!Utils.isNetworkAvailable(ScheduleChatActivity.this)) {
                        Utils.showShortToast(getResources().getString(R.string.no_internet), ScheduleChatActivity.this);
                        //progressBar.setVisibility(View.GONE);
                        return;
                    }


                    if (chat_text.getText().toString().trim().length() != 0) {

                        message_data_obj.put("schedule_msg", chat_text.getText().toString());
                        message_data_obj.put("schedule_date", date_details.getText().toString());
                        message_data_obj.put("schedule_time", selected_time.getText().toString());
                        shcedule_text_list.add(message_data_obj);

                        Log.e("SchedlueChat", "onClick: " + ScheduleText.scheduledMsgs);


                        if (dateSelected && timeSelected && contcatSelected) {

                            String group_id = UUID.randomUUID().toString();

                            String date_Time = userSelectedDate + houOfDay24Format + minute24Format;

                            //String myDate = "10/20/2017 8:10 AM";
                            myDate = userSelectedDate + " " + selectedTime;

                            Log.e(TAG, "userSelectedDate: " + userSelectedDate);
                            Log.e(TAG, "selectedTime: " + userSelectedTime);

                            String convertDateToMilli = userSelectedDate + " " + userSelectedTime;
                            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm a", Locale.ENGLISH);
                            Date date = sdf.parse(convertDateToMilli);
                            long millis = date.getTime();


                            Log.e("miliSecsDate", " = " + millis);


                            Log.e(TAG, "onClickScheduleData: " + jsonObject);
                            for (int i = 0; i < jsonObject.length(); i++) {
                                dbHandler.AddScheduleMessage(new ScheduleTextModel(jsonObject.optJSONObject(i).optString("zoeChatId"), group_id, millis, SharedHelper.getKey(ScheduleChatActivity.this, "id"), chat_text.getText().toString(), "Pending", jsonObject.optJSONObject(i).optString("name")));

                            }


                            sendScheduleText("", chat_text.getText().toString(), group_id, String.valueOf(millis), ChatType.text, "0", "", "", "", "");

//
//


                        } else {
                            if (!dateSelected) {
                                Toast.makeText(ScheduleChatActivity.this, "Please select date", Toast.LENGTH_SHORT).show();
                            } else if (!contcatSelected) {
                                Toast.makeText(ScheduleChatActivity.this, "Please select atleast 1 contact", Toast.LENGTH_SHORT).show();
                            } else if (!timeSelected) {
                                Toast.makeText(ScheduleChatActivity.this, "Please select time", Toast.LENGTH_SHORT).show();
                            }
                        }
                    } else {
                        Toast.makeText(ScheduleChatActivity.this, "Enter message", Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (ParseException e) {

                }


            }
        });

        smileyLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                emojiPopup.toggle();

            }
        });

//        hideImage.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                plusImage.setVisibility(View.VISIBLE);
//                hideImage.setVisibility(View.GONE);
//                bottomlayout2.setVisibility(View.GONE);
//
//            }
//        });

        day1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                dateSelected = true;

                userSelectedDate = days.get(0);
                date_details.setText(days.get(0) + ",");

                //  day1.setBackground(getResources().getDrawable(R.drawable.gradient_color_circle));
                setDateBackground1(Integer.parseInt(themevalue));
                day2.setBackground(getResources().getDrawable(R.drawable.circle_grey));
                day3.setBackground(getResources().getDrawable(R.drawable.circle_grey));
                day4.setBackground(getResources().getDrawable(R.drawable.circle_grey));
                day5.setBackground(getResources().getDrawable(R.drawable.circle_grey));
                day6.setBackground(getResources().getDrawable(R.drawable.circle_grey));
                day7.setBackground(getResources().getDrawable(R.drawable.circle_grey));

            }
        });


        day2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dateSelected = true;
                userSelectedDate = days.get(1);
                date_details.setText(days.get(1) + ",");
                day1.setBackground(getResources().getDrawable(R.drawable.circle_grey));
                //day2.setBackground(getResources().getDrawable(R.drawable.gradient_color_circle));
                setDateBackground2(Integer.parseInt(themevalue));
                day3.setBackground(getResources().getDrawable(R.drawable.circle_grey));
                day4.setBackground(getResources().getDrawable(R.drawable.circle_grey));
                day5.setBackground(getResources().getDrawable(R.drawable.circle_grey));
                day6.setBackground(getResources().getDrawable(R.drawable.circle_grey));
                day7.setBackground(getResources().getDrawable(R.drawable.circle_grey));
            }
        });


        day3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dateSelected = true;
                userSelectedDate = days.get(2);
                date_details.setText(days.get(2) + ",");
                day1.setBackground(getResources().getDrawable(R.drawable.circle_grey));
                day2.setBackground(getResources().getDrawable(R.drawable.circle_grey));
                //day3.setBackground(getResources().getDrawable(R.drawable.gradient_color_circle));
                setDateBackground3(Integer.parseInt(themevalue));
                day4.setBackground(getResources().getDrawable(R.drawable.circle_grey));
                day5.setBackground(getResources().getDrawable(R.drawable.circle_grey));
                day6.setBackground(getResources().getDrawable(R.drawable.circle_grey));
                day7.setBackground(getResources().getDrawable(R.drawable.circle_grey));
            }
        });


        day4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dateSelected = true;
                userSelectedDate = days.get(3);
                date_details.setText(days.get(3) + ",");

                day1.setBackground(getResources().getDrawable(R.drawable.circle_grey));
                day2.setBackground(getResources().getDrawable(R.drawable.circle_grey));
                day3.setBackground(getResources().getDrawable(R.drawable.circle_grey));
//                day4.setBackground(getResources().getDrawable(R.drawable.gradient_color_circle));
                setDateBackground4(Integer.parseInt(themevalue));
                day5.setBackground(getResources().getDrawable(R.drawable.circle_grey));
                day6.setBackground(getResources().getDrawable(R.drawable.circle_grey));
                day7.setBackground(getResources().getDrawable(R.drawable.circle_grey));
            }
        });


        day5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dateSelected = true;
                userSelectedDate = days.get(4);
                date_details.setText(days.get(4) + ",");
                day1.setBackground(getResources().getDrawable(R.drawable.circle_grey));
                day2.setBackground(getResources().getDrawable(R.drawable.circle_grey));
                day3.setBackground(getResources().getDrawable(R.drawable.circle_grey));
                day4.setBackground(getResources().getDrawable(R.drawable.circle_grey));
//                day5.setBackground(getResources().getDrawable(R.drawable.gradient_color_circle));
                setDateBackground5(Integer.parseInt(themevalue));
                day6.setBackground(getResources().getDrawable(R.drawable.circle_grey));
                day7.setBackground(getResources().getDrawable(R.drawable.circle_grey));
            }
        });


        day6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dateSelected = true;
                userSelectedDate = days.get(5);
                date_details.setText(days.get(5) + ",");
                day1.setBackground(getResources().getDrawable(R.drawable.circle_grey));
                day2.setBackground(getResources().getDrawable(R.drawable.circle_grey));
                day3.setBackground(getResources().getDrawable(R.drawable.circle_grey));
                day4.setBackground(getResources().getDrawable(R.drawable.circle_grey));
                day5.setBackground(getResources().getDrawable(R.drawable.circle_grey));
//                day6.setBackground(getResources().getDrawable(R.drawable.gradient_color_circle));
                setDateBackground6(Integer.parseInt(themevalue));
                day7.setBackground(getResources().getDrawable(R.drawable.circle_grey));
            }
        });


        day7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dateSelected = true;
                userSelectedDate = days.get(6);
                date_details.setText(days.get(6) + ",");
                day1.setBackground(getResources().getDrawable(R.drawable.circle_grey));
                day2.setBackground(getResources().getDrawable(R.drawable.circle_grey));
                day3.setBackground(getResources().getDrawable(R.drawable.circle_grey));
                day4.setBackground(getResources().getDrawable(R.drawable.circle_grey));
                day5.setBackground(getResources().getDrawable(R.drawable.circle_grey));
                day6.setBackground(getResources().getDrawable(R.drawable.circle_grey));
//                day7.setBackground(getResources().getDrawable(R.drawable.gradient_color_circle));
                setDateBackground7(Integer.parseInt(themevalue));
            }
        });


        findViewById(R.id.select_contacts).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent group = new Intent(ScheduleChatActivity.this, NewGroup_activity.class);
                ChatFragment.isGroup = "false";
                ChatFragment.isSchedule = "true";
                group.putExtra("forward_msg", "false");
                // group.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivityForResult(group, 100);

            }
        });


        select_time_linear = (LinearLayout) findViewById(R.id.select_time_linear);
        plus_layout = (LinearLayout) findViewById(R.id.plus_layout);
        closeLayout = (LinearLayout) findViewById(R.id.closeLayout);
        select_time_linear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                timeSet();

            }
        });


        plus_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeLayout.setVisibility(View.VISIBLE);
                plus_layout.setVisibility(View.GONE);

                card_visibility.setVisibility(View.VISIBLE);
            }
        });

        closeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeLayout.setVisibility(View.GONE);
                plus_layout.setVisibility(View.VISIBLE);

                card_visibility.setVisibility(View.GONE);
            }
        });


    }


    public static void goBack(Activity context) {
        context.finish();
    }


    // send the message
    public void sendScheduleText(String userID, String msg, String uniqueID, String deliverTime, ChatType
            chatType, String chatRoomType, String groupId, String cName, String cNumber, String shouldSign) {


        ServiceClasss.Emitters emitters = new ServiceClasss.Emitters(ScheduleChatActivity.this);
        AsyncTask<Void, Void, Void> obj = null;
        emitters.sendScheduleText(userID, msg, uniqueID, deliverTime, chatType, chatRoomType, groupId, cName, cNumber, shouldSign, "", "", "", false, obj, 0, 0, ScheduleChatActivity.this);


    }


    private void setUpEmojiPopup() {
        emojiPopup = EmojiPopup.Builder.fromRootView(rootView).setOnEmojiBackspaceClickListener(new OnEmojiBackspaceClickListener() {
            @Override
            public void onEmojiBackspaceClicked(final View v) {
                Log.d("ScheduleChatActivity", "Clicked on Backspace");
            }
        }).setOnEmojiClickedListener(new OnEmojiClickedListener() {
            @Override
            public void onEmojiClicked(final Emoji emoji) {
                Log.d("ScheduleChatActivity", "Clicked on emoji");
            }
        }).setOnEmojiPopupShownListener(new OnEmojiPopupShownListener() {
            @Override
            public void onEmojiPopupShown() {
                plusImage.setImageResource(R.drawable.ic_keyboard_hide);
            }
        }).setOnSoftKeyboardOpenListener(new OnSoftKeyboardOpenListener() {
            @Override
            public void onKeyboardOpen(final int keyBoardHeight) {
                Log.d("ScheduleChatActivity", "Opened soft keyboard");
            }
        }).setOnEmojiPopupDismissListener(new OnEmojiPopupDismissListener() {
            @Override
            public void onEmojiPopupDismiss() {
                plusImage.setImageResource(R.drawable.smiley_img);
            }
        }).setOnSoftKeyboardCloseListener(new OnSoftKeyboardCloseListener() {
            @Override
            public void onKeyboardClose() {
                emojiPopup.dismiss();
            }
        }).build(chat_text);
    }

    public void timeSet() {

        // Get Current Time
        final Calendar c = Calendar.getInstance();
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);

        // Launch Time Picker Dialog
        final TimePickerDialog timePickerDialog = new TimePickerDialog(this, R.style.DialogTheme,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        houOfDay24Format = hourOfDay;
                        minute24Format = minute;
                        Log.e(TAG, "houOfDay24Format: " + houOfDay24Format);
                        Log.e(TAG, "minute24Format: " + minute24Format);
                        String status = "AM";


                        if (hourOfDay > 11) {
                            // If the hour is greater than or equal to 12
                            // Then the current AM PM status is PM
                            status = "PM";
                        }

                        // Initialize a new variable to hold 12 hour format hour value
                        int hour_of_12_hour_format;

                        if (hourOfDay > 11) {

                            // If the hour is greater than or equal to 12
                            // Then we subtract 12 from the hour to make it 12 hour format time
                            hour_of_12_hour_format = hourOfDay - 12;
                        } else {
                            hour_of_12_hour_format = hourOfDay;
                        }


                        if (hour_of_12_hour_format == 0) {
                            hour_of_12_hour_format = 12;
                        }

                        String hourString;
                        if (hour_of_12_hour_format < 10)
                            hourString = "0" + hour_of_12_hour_format;
                        else
                            hourString = "" + hour_of_12_hour_format;

                        String minuteSting;
                        if (minute < 10)
                            minuteSting = "0" + minute;
                        else
                            minuteSting = "" + minute;


                        //Validate the time only for current day.. user should not select the past time

                        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
                        String currentDate = df.format(c.getTime());

                        if (currentDate.equals(userSelectedDate)) {
                            Calendar ca = Calendar.getInstance(Locale.getDefault());
                            ca.add(Calendar.MINUTE, 10);


                            if ((hourOfDay <= (ca.get(Calendar.HOUR_OF_DAY))) &&
                                    (minute <= (ca.get(Calendar.MINUTE)))) {
                                Toast.makeText(ScheduleChatActivity.this, "Wrong time! Please select the time 10 minutes greater than current time",
                                        Toast.LENGTH_SHORT).show();
                            } else {
                                userSelectedTime = hourString + ":" + minuteSting + " " + status;
                                selectedTime = hourString + ":" + minuteSting + ": " + status + ", ";
                                Log.e("Current Time", hourString + " : " + minuteSting + " : " + status + " ");
                                time_details.setText(selectedTime);
                                selected_time.setText(selectedTime);
                                timeSelected = true;
                            }
                        } else {
                            userSelectedTime = hourString + ":" + minuteSting + " " + status;
                            selectedTime = hourString + ":" + minuteSting + ": " + status + ", ";
                            Log.e("Current Time", hourString + " : " + minuteSting + " : " + status + " ");
                            time_details.setText(selectedTime);
                            selected_time.setText(selectedTime);
                            timeSelected = true;
                        }

                    }
                }, mHour, mMinute, false);

        timePickerDialog.show();


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {


        closeLayout.setVisibility(View.GONE);
        plus_layout.setVisibility(View.VISIBLE);

        if (card_visibility.getVisibility() == View.VISIBLE) {
            card_visibility.setVisibility(GONE);
        }

        try {
            String list = data.getStringExtra("part_list");
            String select_list = data.getStringExtra("select_list");
            Log.d("ScheduleListSize", "" + list);
            Log.d("ParticiSchedule", "" + select_list);
            jsonObject = new JSONArray(list);

            if (jsonObject.length() == 0) {
                contacts_details.setText("0" + " Contacts");
            } else {
                contcatSelected = true;
                contacts_details.setText(jsonObject.length() + " Contacts");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}