package com.app.fizzychat.activity;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.app.fizzychat.DBHelper.DBHandler;
import com.app.fizzychat.R;
import com.app.fizzychat.Service.ServiceClasss;
import com.app.fizzychat.baseUtils.AppController;
import com.app.fizzychat.baseUtils.AsyncTaskCompleteListener;
import com.app.fizzychat.baseUtils.Const;
import com.app.fizzychat.baseUtils.MyCommon;
import com.app.fizzychat.baseUtils.PostHelper;
import com.app.fizzychat.baseUtils.SharedHelper;
import com.app.fizzychat.baseUtils.Utils;
import com.app.fizzychat.fragment.CallsFragment;
import com.app.fizzychat.fragment.ChatFragment;
import com.app.fizzychat.fragment.ContactFragment;
import com.app.fizzychat.models.ChannelParticiapntsModel;
import com.app.fizzychat.models.ChatType;
import com.app.fizzychat.models.GroupParticiapntsModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainActivity extends AppCompatActivity implements ChatFragment.count {

    public static MainActivity mainActivity;
    public static int unreadCount = 0;
    public static Socket socket;
    public static int value = 0;
    public static Boolean isSending = false;
    public static String my_id;
    public static String my_country_code;
    public String groupId_loc;
    public ProgressBar progressBar;
    public static TextView counterTextView;
    public static Toolbar toolbar;
    Emitter.Listener onDisconnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {

            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("id", SharedHelper.getKey(getApplicationContext(), "id"));
                socket.emit("offline", jsonObject);
                Log.e("offline", "" + jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
//            socket.disconnect();
        }
    };
    
    private Handler handler1;
    //private DBHandler dbHandler;
    public static ViewPager mViewPager;
    TabLayout tabLayout;
    private SectionsPagerAdapter mSectionsPagerAdapter;
    String[] tabTitle = {"CHATS", "CONTACTS", "CALLS"};
    int[] unreadCountNew = {5, 0, 0};
    public static TextView unread_chatcount;
    private List<Fragment> FragmentList = new ArrayList();
    private List<String> FragmentTitle = new ArrayList();
    private Emitter.Listener onConnectError = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            JSONObject jsonObject = (JSONObject) args[0];
            Log.e("MainActivity", "Socket_Error" + jsonObject);
        }
    };
    //    private Emitter.Listener onConnect = new Emitter.Listener() {
//        @Override
//        public void call(Object... args) {
//            /*ChatActivity.this.runOnUiThread(new Runnable() {
//                @Override
//                public void run() {*/
//            try {
//                JSONObject jsonObject = new JSONObject();
//                jsonObject.put("id", SharedHelper.getKey(getApplicationContext(), "id"));
//                Log.e("connect", "online" + jsonObject);
//                socket.emit("online", jsonObject);
//
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//             /*   }
//            });*/
//
//        }
//    };
    private Runnable internetCheck = new Runnable() {
        @Override
        public void run() {

            Boolean isAvailable = isNetworkAvailable(getApplicationContext());

            if (isAvailable) {


                if (!isSending) {


                    isSending = true;
//                    sendingmessage();
                }
            }

            handler1.postDelayed(this, 5000);
        }

    };

    private void Setheme(String themevalue) {
        Log.d("Setheme: ", "themevalues:" + themevalue);
        switch (themevalue) {
            case "1":
                setTheme(R.style.AppThemeGreen);
                break;
            case "2":
                setTheme(R.style.AppThemeBlue);
                break;
            case "3":
                setTheme(R.style.AppThemeIndigo);
                break;
            case "4":
                setTheme(R.style.AppThemeGrey);
                break;
            case "5":
                setTheme(R.style.AppThemeYellow);
                break;
            case "6":
                setTheme(R.style.AppThemeOrange);
                break;
            case "7":
                setTheme(R.style.AppThemePurple);
                break;
            case "8":
                setTheme(R.style.AppThemePaleGreen);
                break;
            case "9":
                setTheme(R.style.AppThemelightBlue);
                break;
            case "10":
                setTheme(R.style.AppThemePink);
                break;
            case "11":
                setTheme(R.style.AppThemelightGreen);
                break;
            case "12":
                setTheme(R.style.AppThemelightRed);
                break;
            default:
                setTheme(R.style.AppThemeGreen);
                break;
        }
    }

//    public static void SendChatMsgunsent(String image_path, String msgId, String userID,
//                                         String currentTime, String chatType, String from, String groupId, String chatRoomType) {
//        JSONObject jsonObject = new JSONObject();
//        try {
//            jsonObject.put("from", from);
//
//            if (chatRoomType.equalsIgnoreCase("0")) {
//                jsonObject.put("to", userID);
//            } else if (chatRoomType.equalsIgnoreCase("1")) {
//                jsonObject.put("to", groupId);
//            }
//            jsonObject.put("messageId", msgId);
//            jsonObject.put("chatRoomType", chatRoomType);
//            jsonObject.put("groupId", groupId);
//            jsonObject.put("time", currentTime);
//            jsonObject.put("content", image_path);
//
//            jsonObject.put("contentType", chatType);
//
//            Log.e("sendMsg_from_adapter", jsonObject.toString());
//            socket.emit("sendMessage", jsonObject);
//
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String themevalue = SharedHelper.getKey(this, "theme_value");
        Setheme(themevalue);
        setContentView(R.layout.activity_main);
        my_id = SharedHelper.getKey(MainActivity.this, "id");
        my_country_code = SharedHelper.getKey(MainActivity.this, "country_code_default");
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        Utils.enableStrictMode();

        IO.Options opts = new IO.Options();
        opts.forceNew = true;
        opts.reconnection = false;
        handler1 = new Handler();

        Log.d("onCreate: ", "MainActivity");
        try {
            socket = IO.socket(Const.chatSocketURL, opts);
        } catch (URISyntaxException e) {
            e.printStackTrace();
            Log.e("SOCKET.IO ", e.getMessage());
        }

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setOverflowIcon(getResources().getDrawable(R.drawable.action_menu));
        progressBar = (ProgressBar) toolbar.findViewById(R.id.progress);
        counterTextView = (TextView) toolbar.findViewById(R.id.context_menu);
        setSupportActionBar(toolbar);
        mainActivity = MainActivity.this;

        if (!isMyServiceRunning(ServiceClasss.class)) {
            MainActivity.this.startService(new Intent(MainActivity.this, ServiceClasss.class));
        } else {
            sendingmessage();

        }

        SharedHelper.putKey(MainActivity.this, "firstTimeToApp", "false");
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setOffscreenPageLimit(1);
        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//                Log.d("onPageScrolled: ", "position:" + position);

            }

            @Override
            public void onPageSelected(int position) {
                Log.d("onPageSelected: ", "position:" + position);
//
//                if (position == 0) {
//                    ContactFragment.refresh.clearActionM();
//                    CallsFragment.mainActivity.clearActionM();
//                } else if (position == 1) {
//                    ChatFragment.mainActivity.resetSelection();
//                    CallsFragment.mainActivity.clearActionM();
//                } else {
//                    ChatFragment.mainActivity.resetSelection();
//                    ContactFragment.refresh.clearActionM();
//                }


//                if (position == 0) {
//                  toolbar.getMenu().clear();
//                toolbar.inflateMenu(R.menu.chat_main);
//                    toolbar.setNavigationIcon(null);
//                    counterTextView.setText(R.string.app_name);
//                    try {
//                        new AsyncTask<String, String, String>() {
//                            @Override
//                            protected String doInBackground(String... params) {
//                                ContactFragment.refresh.Refresgfrommain();
//                                CallsFragment.mainActivity.Refreshfrommain();
//                                return "yes";
//                            }
//                        }.execute();
//                    } catch (Exception e) {
//
//                    }
//
//                } else if (position == 1) {
//                   toolbar.getMenu().clear();
//                    toolbar.inflateMenu(R.menu.contact_main);
//                   toolbar.setNavigationIcon(null);
//                    counterTextView.setText(R.string.app_name);
//
//                    try {
//                        new AsyncTask<String, String, String>() {
//                            @Override
//                            protected String doInBackground(String... params) {
//
//                                ChatFragment.mainActivity.Refreshfrommain();
//                                CallsFragment.mainActivity.Refreshfrommain();
//                                return "yes";
//                            }
//                        }.execute();
//                    } catch (Exception e) {
//
//                    }
//
//
//                } else {
//                   toolbar.getMenu().clear();
//                  toolbar.inflateMenu(R.menu.contact_main);
//                   toolbar.setNavigationIcon(null);
//                    counterTextView.setText(R.string.app_name);
//
//                    try {
//                        new AsyncTask<String, String, String>() {
//                            @Override
//                            protected String doInBackground(String... params) {
//                                ChatFragment.mainActivity.Refreshfrommain();
//                                ContactFragment.refresh.Refresgfrommain();
//                                return "yes";
//                            }
//                        }.execute();
//                    } catch (Exception e) {
//
//                    }
//                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        mSectionsPagerAdapter.addFragment(new ChatFragment(), getResources().getString(R.string.chats));
        mSectionsPagerAdapter.addFragment(new ContactFragment(), getResources().getString(R.string.contacts));
        mSectionsPagerAdapter.addFragment(new CallsFragment(), getResources().getString(R.string.calls));

        mViewPager.setAdapter(mSectionsPagerAdapter);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        //tabLayout.setTabTextColors(getResources().getColor(R.color.nav_bg), getResources().getColor(R.color.colorAccent));
        //tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.colorAccent));
        tabLayout.setupWithViewPager(mViewPager);

        updateDeviceToken();


//        try {
//            setupTabIcons();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }

    private void updateDeviceToken() {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("id", SharedHelper.getKey(MainActivity.this, "id"));
            jsonObject.put("pushNotificationId", SharedHelper.getKey(MainActivity.this, "token"));

            Log.e("getting_ph", jsonObject.toString());
            new PostHelper(Const.Methods.UPDATE_TOKEN, jsonObject.toString(), Const.ServiceCode.GET_OTP, this, new AsyncTaskCompleteListener() {
                @Override
                public void onTaskCompleted(JSONObject response, int serviceCode) {

                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
            Utils.removeProgressDialog();
        }
    }


    private View prepareTabView(int pos) {
        View view = getLayoutInflater().inflate(R.layout.custom_tab, null);
        TextView tv_title = (TextView) view.findViewById(R.id.tv_title);
        unread_chatcount = (TextView) view.findViewById(R.id.tv_count);
        tv_title.setText(tabTitle[pos]);
        if (unreadCountNew[pos] > 0) {
            unread_chatcount.setVisibility(View.VISIBLE);
//            unread_chatcount.setText("" );
        }
//        else
//            unread_chatcount.setVisibility(View.GONE);


        return view;
    }

    private void setupTabIcons() {

        for (int i = 0; i < tabTitle.length; i++) {
            /*TabLayout.Tab tabitem = tabLayout.newTab();
            tabitem.setCustomView(prepareTabView(i));
            tabLayout.addTab(tabitem);*/

            tabLayout.getTabAt(i).setCustomView(prepareTabView(i));
        }


    }

    public boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
        }
        return false;
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public void sendChatFailed(String userID, String msg, String uniqueID, String currentTime, ChatType chatType, String chatRoomType, String groupId, String cName, String cNumber, String shouldSign, String mtitle, String mdesc, String mlogo, String shoprev, String lati, String lngi) {
        Boolean prr;
        AsyncTask<Void, Void, Void> asynckobj = null;

        if (shoprev.equalsIgnoreCase("true")) {
            prr = true;
        } else {
            prr = false;
        }
        if (lati.trim().length() == 0) {
            lati = "0";
        }

        if (lngi.trim().length() == 0) {
            lngi = "0";
        }

        ServiceClasss.Emitters emitters = new ServiceClasss.Emitters(MainActivity.this);
        emitters.sendChat(userID, msg, uniqueID, currentTime, chatType, chatRoomType, groupId, cName, cNumber, shouldSign, mtitle, mdesc, mlogo, prr, asynckobj, Double.parseDouble(lati), Double.parseDouble(lngi));


    }

    public void sendingmessage() {
        DBHandler dbHandler = new DBHandler(this);
        JSONArray messages = dbHandler.GetChatsMessagesUnSent();
        if (messages.length() > 0) {
            for (int i = 0; i < messages.length(); i++) {
                JSONObject object = messages.optJSONObject(i);
                String msg = object.optString("msg");
                String msg_id = object.optString("msgId");
                String user_id = object.optString("userId");
                String time = String.valueOf(System.currentTimeMillis());
                String chatType = object.optString("chatType");

                ChatType type = null;

                if ("text".equalsIgnoreCase(chatType)) {
                    type = ChatType.text;
                } else if ("image".equalsIgnoreCase(chatType)) {
                    type = ChatType.image;
                } else if (ChatType.location.equals(chatType)) {
                    type = ChatType.location;
                } else if (ChatType.contact.equals(chatType)) {
                    type = ChatType.contact;
                } else if (ChatType.video.equals(chatType)) {
                    type = ChatType.video;
                }

                String from = SharedHelper.getKey(getApplicationContext(), "id");
                String grp_id = object.optString("groupId");
                String chatrromtype = object.optString("chatRoomType");
                String showpreview = object.optString("showPreview");
                String title = "", description = "", logo = "", lat = "", longg = "", c_name = "", c_number = "";

                if (showpreview.equalsIgnoreCase("true")) {
                    title = object.optString("metaTitle");
                    description = object.optString("metaDescription");
                    logo = object.optString("metaLogo");
                }
                if (chatType.equals(ChatType.location)) {
                    lat = object.optString("lat");
                    longg = object.optString("lng");
                } else if (chatType.equals(ChatType.contact)) {
                    c_name = object.optString("cName");
                    c_number = object.optString("cNumber");
                }


                if (i == messages.length()) {
                    isSending = false;
                    MainActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Log.d("run: ", "notifying_adapter");
                            ChatActivity.singleChatAdapter.notifyDataSetChanged();
                        }
                    });
                }
                String upload = object.optString("upload");

                if (upload.equalsIgnoreCase("3")) {

                    String sign = dbHandler.GetSign(groupId_loc);
                    sendChatFailed(user_id, msg, msg_id, time, type, chatrromtype, grp_id, c_name, c_number, sign, title, description, logo, showpreview, lat, longg);
                } else {

                    sendChatFailed(user_id, msg, msg_id, time, type, chatrromtype, grp_id, c_name, c_number, "true", title, description, logo, showpreview, lat, longg);
                }

            }
        }
    }
    //imgpath,msgid,userId,currentime,chattype,from,groupid,chatroomtype


    @Override
    protected void onResume() {
        super.onResume();
        isSending = false;
//        ContactFragment.refresh.Refresgfrommain();
        handler1.post(internetCheck);
        if (!isMyServiceRunning(ServiceClasss.class)) {
            MainActivity.this.startService(new Intent(MainActivity.this, ServiceClasss.class));
        }
        MyCommon.getInstance().mainActivity = true;
        /*if (!SocketSingleton.getInstance().socket.connected()) {
            SocketSingleton.getInstance().onConnectSocket();
            Log.e("Scoket", "Connect");
        } else {
            Log.e("Scoket", "notConnect");
        }*/

        Log.d("onResume: ", "MainActivity");
//        ChatFragment.refreshLay();
        ChatActivity.zoeChatID = "";
        ChatActivity.groupId_loc = "";

//        socket.connect();
//        socket.on(Socket.EVENT_CONNECT, onConnect);


    }

    @Override
    protected void onPause() {
        super.onPause();
//        handler1.removeCallbacks(internetCheck);
        isSending = false;
        MyCommon.getInstance().mainActivity = false;
//        JSONObject jsonObject = new JSONObject();
//        try {
//            jsonObject.put("id", SharedHelper.getKey(getApplicationContext(), "id"));
//            socket.emit("offline", jsonObject);
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        socket.disconnect();
    }


    @Override
    public void onBackPressed() {


        if (AppController.is_in_action_mode) {
            ChatFragment.mainActivity.resetToolbarAndCount();
            CallsFragment.mainActivity.clearActionM();

        } else {

            if (mViewPager.getCurrentItem() != 0) {
                mViewPager.setCurrentItem(0);
            } else
                super.onBackPressed();
        }
    }

    private void validateincomingvalues(JSONArray particpants_array) {
        DBHandler dbHandler = new DBHandler(MainActivity.this);
        for (int i = 0; i < particpants_array.length(); i++) {
            String id = particpants_array.optJSONObject(i).optString("participantId");
            String grp_id = particpants_array.optJSONObject(i).optString("groupId");
            Boolean exist = dbHandler.CheckParticipantAlreadyInDBorNot(id, grp_id);
            if (exist) {
                String isAdmin = particpants_array.optJSONObject(i).optString("isAdmin");
                if (isAdmin.equalsIgnoreCase("")) {
                    isAdmin = "0";
                }
                dbHandler.groupPartiUpdate(grp_id, particpants_array.optJSONObject(i).optString("joinedAt"), isAdmin, particpants_array.optJSONObject(i).optString("addedBy"), id);
            } else {
                String isAdmin = particpants_array.optJSONObject(i).optString("isAdmin");
                if (isAdmin.equalsIgnoreCase("")) {
                    isAdmin = "0";
                }
                dbHandler.AddGroupParticipants(new GroupParticiapntsModel(id, grp_id, particpants_array.optJSONObject(i).optString("joinedAt"), particpants_array.optJSONObject(i).optString("addedBy"), isAdmin));
            }

        }


    }

    private void validatelocalvalues(List<GroupParticiapntsModel> models, JSONArray particpants_array) {
        DBHandler dbHandler = new DBHandler(this);
        for (int i = 0; i < models.size(); i++) {
            GroupParticiapntsModel values = models.get(i);
            String id = values.getUser_id();
            Boolean exist = value(particpants_array, id);
            if (!exist) {
                dbHandler.DeleteGroupParticipants(id, groupId_loc);
            }
        }

    }


    private void cvalidateincomingvalues(JSONArray particpants_array) {
        DBHandler dbHandler = new DBHandler(MainActivity.this);
        for (int i = 0; i < particpants_array.length(); i++) {
            String id = particpants_array.optJSONObject(i).optString("participantId");
            String grp_id = particpants_array.optJSONObject(i).optString("groupId");
            Boolean exist = dbHandler.CheckChanneParticipantAlreadyInDBorNot(id, grp_id);
            if (exist) {
                String isAdmin = particpants_array.optJSONObject(i).optString("isAdmin");
                if (isAdmin.equalsIgnoreCase("")) {
                    isAdmin = "user";
                }
                dbHandler.channelPartiUpdate(grp_id, particpants_array.optJSONObject(i).optString("joinedAt"), isAdmin, particpants_array.optJSONObject(i).optString("addedBy"), id);
            } else {
                String isAdmin = particpants_array.optJSONObject(i).optString("isAdmin");
                if (isAdmin.equalsIgnoreCase("")) {
                    isAdmin = "user";
                }
                dbHandler.AddChannelParticipants(new ChannelParticiapntsModel(id, grp_id, particpants_array.optJSONObject(i).optString("joinedAt"), particpants_array.optJSONObject(i).optString("addedBy"), isAdmin));
            }

        }


    }

    private void cvalidatelocalvalues(List<ChannelParticiapntsModel> models, JSONArray particpants_array) {
        DBHandler dbHandler = new DBHandler(this);
        for (int i = 0; i < models.size(); i++) {
            ChannelParticiapntsModel values = models.get(i);
            String id = values.getUser_id();
            Boolean exist = value(particpants_array, id);
            if (!exist) {
                dbHandler.DeleteChannelParticipants(id, groupId_loc);
            }
        }

    }

    private boolean value(JSONArray jsonArray, String part_id) {
        return jsonArray.toString().contains("\"participantId\":\"" + part_id + "\"");
    }

    @Override
    public void getCount(int value) {
        // Log.e("MainAct", "getCount:"+value );
        //unread_chatcount.setText(String.valueOf(value));
    }

    private class SectionsPagerAdapter extends FragmentPagerAdapter {
        public SectionsPagerAdapter(FragmentManager supportFragmentManager) {
            super(supportFragmentManager);

        }

        public void addFragment(Fragment fragment, String title) {
            FragmentList.add(fragment);
            FragmentTitle.add(title);
        }

        @Override
        public Fragment getItem(int position) {
            return FragmentList.get(position);
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return FragmentTitle.get(position);
        }
    }

    private class getGrpdetails extends AsyncTask<String, String, String> implements AsyncTaskCompleteListener {
        String result = "";
        private JSONArray particpants_array = new JSONArray();
        private JSONObject group_details = new JSONObject();

        @Override
        protected String doInBackground(String... params) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("groupId", groupId_loc);

                new PostHelper(Const.Methods.PARTICIPANTS_DETAILS, jsonObject.toString(), Const.ServiceCode.PARTICIPANTS_DETAILS, MainActivity.this, this);

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return result;
        }


        @Override
        public void onTaskCompleted(JSONObject response, int serviceCode) {

            DBHandler dbHandler = new DBHandler(MainActivity.this);
            particpants_array = response.optJSONArray("participants");
            group_details = response.optJSONObject("groupDetails");


            List<GroupParticiapntsModel> models = new ArrayList<>();

            if (dbHandler.DoesChatExist(groupId_loc)) {
                models = dbHandler.GetPartiFromGrp(groupId_loc);

                validatelocalvalues(models, particpants_array);
                validateincomingvalues(particpants_array);
            } else {
                for (int i = 0; i < particpants_array.length(); i++) {
                    String id = particpants_array.optJSONObject(i).optString("participantId");
                    String grp_id = particpants_array.optJSONObject(i).optString("groupId");
                    String isAdmin = particpants_array.optJSONObject(i).optString("isAdmin");
                    if (isAdmin.equalsIgnoreCase("")) {
                        isAdmin = "0";
                    }


                    dbHandler.AddGroupParticipants(new GroupParticiapntsModel(id, grp_id, particpants_array.optJSONObject(i).optString("joinedAt"), particpants_array.optJSONObject(i).optString("addedBy"), isAdmin));

                }
            }


            Log.d("onTaskCompleted: ", "" + response);

        }
    }

    private class getChanneldetails extends AsyncTask<String, String, String> implements AsyncTaskCompleteListener {
        String result = "";
        private JSONArray particpants_array = new JSONArray();
        private JSONObject group_details = new JSONObject();

        @Override
        protected String doInBackground(String... params) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("channelId", groupId_loc);

                new PostHelper(Const.Methods.CHANNEL_PARTICIPANTS_DETAILS, jsonObject.toString(), Const.ServiceCode.CHANNEL_PARTICIPANTS_DETAILS, MainActivity.this, this);

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return result;
        }


        @Override
        public void onTaskCompleted(JSONObject response, int serviceCode) {

            DBHandler dbHandler = new DBHandler(MainActivity.this);
            particpants_array = response.optJSONArray("participants");
            group_details = response.optJSONObject("groupDetails");


            List<ChannelParticiapntsModel> models = new ArrayList<>();

            if (dbHandler.DoesChatExist(groupId_loc)) {
                models = dbHandler.GetPartiFromChannel(groupId_loc);

                cvalidatelocalvalues(models, particpants_array);
                cvalidateincomingvalues(particpants_array);
            } else {
                for (int i = 0; i < particpants_array.length(); i++) {
                    String id = particpants_array.optJSONObject(i).optString("participantId");
                    String grp_id = particpants_array.optJSONObject(i).optString("groupId");
                    String isAdmin = particpants_array.optJSONObject(i).optString("isAdmin");
                    if (isAdmin.equalsIgnoreCase("")) {
                        isAdmin = "user";
                    }


                    dbHandler.AddChannelParticipants(new ChannelParticiapntsModel(id, grp_id, particpants_array.optJSONObject(i).optString("joinedAt"), particpants_array.optJSONObject(i).optString("addedBy"), isAdmin));

                }
            }


            Log.d("onTaskCompleted: ", "" + response);

        }
    }


}
