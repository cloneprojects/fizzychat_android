package com.app.fizzychat.baseUtils;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;
import android.util.TypedValue;

import com.app.fizzychat.DBHelper.DBHandler;
import com.app.fizzychat.R;
import com.app.fizzychat.activity.VideoChatViewActivity;
import com.app.fizzychat.models.CallsModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;


/**
 * Created by pyramidion on 2/23/18.
 */

public class MakeCalls {


    private static void isAppIsInBackground(Context context) {
        boolean isInBackground = true;
        SharedHelper.putKey(context, "isBackground", "true");
        try {
            ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
                List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
                for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                    if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                        for (String activeProcess : processInfo.pkgList) {
                            if (activeProcess.equals(context.getPackageName())) {
                                isInBackground = false;
                                SharedHelper.putKey(context, "isBackground", "false");

                            }
                        }
                    }
                }
            } else {
                List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
                ComponentName componentInfo = taskInfo.get(0).topActivity;
                if (componentInfo.getPackageName().equals(context.getPackageName())) {
                    isInBackground = false;
                    SharedHelper.putKey(context, "isBackground", "false");

                }
            }
        } catch (Exception e) {

        }

//        Log.d(TAG, "isAppIsInBackground: " + isInBackground);

    }


    public static int getPrimaryCOlor(Activity activity) {
        final TypedValue value = new TypedValue();
        activity.getTheme().resolveAttribute(R.attr.colorPrimary, value, true);
        return value.data;
    }

    public static int getPrimaryDark(Activity activity) {
        final TypedValue value = new TypedValue();
        activity.getTheme().resolveAttribute(R.attr.colorPrimaryDark, value, true);
        return value.data;
    }



    public void makeVoiceCall(final Activity activity, final String zoeChatID, final String name, final String image) {

        SharedHelper.putKey(activity, "CallType", "VoiceCall");
        final DBHandler dbHandler=new DBHandler(activity);

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("from", SharedHelper.getKey(activity, "id"));
            jsonObject.put("to", zoeChatID);
            String channel = SharedHelper.getKey(activity, "id");
            jsonObject.put("channelId", channel);
            jsonObject.put("isVideoCall", "false");
            new PostHelper(Const.Methods.CALL, jsonObject.toString(), Const.ServiceCode.CALL_CODE, activity, new AsyncTaskCompleteListener() {
                @Override
                public void onTaskCompleted(JSONObject response, int serviceCode) {
                    Log.d("Makecalls", "onTaskCompleted: "+response);
                    if (response != null) {

                        if (response.optString("error").equalsIgnoreCase("false")) {
                            dbHandler.InsertCalls(new CallsModel(zoeChatID, name, image, System.currentTimeMillis(), "VoiceCall", "0"));
                            Intent call = new Intent(activity, VideoChatViewActivity.class);
                            call.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            call.putExtra("zoeChatID", zoeChatID);
                            call.putExtra("user_name", name);
                            call.putExtra("image", image);
                            call.putExtra("receive", "no");
                            call.putExtra("call", "voiceCall");
                            activity.startActivity(call);
                        } else {
                            Utils.showShortToast(response.optString("message"), activity);
                        }
                    }

                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    public void makeVideoCall(final Activity activity, final String zoeChatID, final String name, final String image) {

        SharedHelper.putKey(activity, "CallType", "VideoCall");

        final DBHandler dbHandler=new DBHandler(activity);

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("from", SharedHelper.getKey(activity, "id"));
            jsonObject.put("to", zoeChatID);
            String channel = SharedHelper.getKey(activity, "id");
            jsonObject.put("channelId", channel);
            jsonObject.put("isVideoCall", "true");
            new PostHelper(Const.Methods.CALL, jsonObject.toString(), Const.ServiceCode.CALL_CODE, activity, new AsyncTaskCompleteListener() {
                @Override
                public void onTaskCompleted(JSONObject response, int serviceCode) {
                    Log.d("Makecalls", "onTaskCompleted: "+response);

                    if (response != null) {
                        if (response.optString("error").equalsIgnoreCase("false")) {
                            dbHandler.InsertCalls(new CallsModel(zoeChatID, name, image, System.currentTimeMillis(), "VideoCall", "0"));
                            Intent call = new Intent(activity, VideoChatViewActivity.class);
                            call.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            call.putExtra("zoeChatID", zoeChatID);
                            call.putExtra("user_name", name);
                            call.putExtra("image", image);
                            call.putExtra("call", "videoCall");
                            call.putExtra("receive", "no");
                            activity.startActivity(call);
                        } else {
                            Utils.showShortToast(response.optString("message"), activity);
                        }
                    }
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }



  /*  public  void setIconColour(Drawable drawable, int primaryCOlor) {
        drawable.setColorFilter(new PorterDuffColorFilter(getPrimaryCOlor(activity), PorterDuff.Mode.SRC_IN));

    }*/


}
