package com.app.fizzychat.Service;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.RemoteInput;
import android.util.Log;


import com.app.fizzychat.DBHelper.DBHandler;
import com.app.fizzychat.activity.ChatActivity;
import com.app.fizzychat.activity.MainActivity;
import com.app.fizzychat.baseUtils.SharedHelper;
import com.app.fizzychat.baseUtils.Utils;
import com.app.fizzychat.models.ChatMessages;
import com.app.fizzychat.models.ChatType;
import com.app.fizzychat.models.ChatsMessagesModel;
import com.app.fizzychat.models.ChatsModel;
import com.app.fizzychat.models.Status;

import java.text.ParseException;
import java.util.UUID;


public class NotificationReceiver extends BroadcastReceiver {
    public String NOTIFICATION_REPLY = "notification_reply";
    private String userIds;
    private String chatRoomIds;
    private int value;
    private double lat = 0;
    private double lng = 0;
    private boolean showPreview = false;
    private String title = "";
    private String description = "";
    private String logo = "";

    public NotificationReceiver() {
    }



    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO: This method is called when the BroadcastReceiver is receiving
        // an Intent broadcast.
        Bundle remoteInput = RemoteInput.getResultsFromIntent(intent);
        Bundle check = intent.getExtras();


        Bundle values = RemoteInput.getResultsFromIntent(intent);

        if (values==null)
        {
            Intent intent1=new Intent(context, MainActivity.class);
            context.startActivity(intent1);

        } else {
            if (remoteInput != null) {

                try {

                    userIds = intent.getExtras().getString("userId");
                    chatRoomIds = intent.getExtras().getString("chatroomtype");

                    Log.e("NotificationReceivers", "onReceives: " + userIds + "charooms-->" + chatRoomIds);

                } catch (Exception e) {
                    e.printStackTrace();
                }

                //Toast.makeText(context, "", Toast.LENGTH_SHORT).show();


                CharSequence messageContent = remoteInput.getCharSequence(NOTIFICATION_REPLY);


                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
                        .setSmallIcon(android.R.drawable.ic_menu_info_details)
                        .setContentTitle("Message sent --- " + messageContent);

                NotificationManager notificationManager = (NotificationManager) context.
                        getSystemService(Context.NOTIFICATION_SERVICE);
                notificationManager.notify(112, mBuilder.build());
                ChatActivity chatActivity = new ChatActivity();
                String grpId = "";


                if (chatRoomIds.equalsIgnoreCase("0")) {
                    sendNotificationMsg(userIds, messageContent.toString(), String.valueOf(UUID.randomUUID()), String.valueOf(System.currentTimeMillis()), ChatType.text, ChatMessages.SENDER, "", grpId, "", "", chatRoomIds, "", "", "", context);
                } else {

                }

                cancelNotification(context, 112);

            }
        }
    }


    public static void cancelNotification(Context ctx, int notifyId) {
        String ns = Context.NOTIFICATION_SERVICE;
        NotificationManager nMgr = (NotificationManager) ctx.getSystemService(ns);
        nMgr.cancel(notifyId);
    }


    public void sendNotificationMsg(String userID, String msg, String uniqueID, String currentTime, ChatType chatType,
                        String sender, String groupName, String groupId, String create_by, String sent_by, String chatRoomType, String cName, String cNumber, String shouldSign, Context context) {

        //long time = dbHandler.GetLastRow(userID);
        long time = Long.parseLong(SharedHelper.getHeader(context, "header_time" + userID));

        Log.e("lastRowTime", String.valueOf(+time));
        try {
            String chatTime = Utils.formatToYesterdayOrToday(Utils.getDate(time, "dd/MM/yyyy hh:mm:ss.SSS"));
            Log.e("chatTime", chatTime);

            if (time == 0) {
                SharedHelper.putHeader(context, "header_time" + userID, currentTime);
                addNotifiHeader(ChatMessages.HEADER, currentTime, String.valueOf(System.currentTimeMillis()), chatRoomType, groupId, context);
                addMessgeToDb(userID, msg, uniqueID, currentTime, sender, chatType, groupName, create_by, sent_by, groupId, chatRoomType, cName, cNumber, shouldSign, context);

            } else if (chatTime.equalsIgnoreCase("Today")) {
                if (value == 0) {
                    value++;
                    SharedHelper.putHeader(context, "header_time" + userID, currentTime);
                    SharedHelper.putInt(context, "date", value);
                }
                value = Integer.parseInt(SharedHelper.getInt(context, "date"));
                SharedHelper.putHeader(context, "header_time" + userID, currentTime);
                addMessgeToDb(userID, msg, uniqueID, currentTime, sender, chatType, groupName, create_by, sent_by, groupId, chatRoomType, cName, cNumber, shouldSign, context);

            } else {
                SharedHelper.putHeader(context, "header_time" + userID, currentTime);
                addNotifiHeader(ChatMessages.HEADER, currentTime, String.valueOf(System.currentTimeMillis()), chatRoomType, groupId, context);
                addMessgeToDb(userID, msg, uniqueID, currentTime, sender, chatType, groupName, create_by, sent_by, groupId, chatRoomType, cName, cNumber, shouldSign, context);
                value = 0;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (chatType.equals(ChatType.text)) {
            sendChatMessage(userID, msg, uniqueID, currentTime, chatType, chatRoomType, groupId, cName, cNumber, shouldSign, context);
        } else if (chatType.equals(ChatType.location)) {
            sendChatMessage(userID, msg, uniqueID, currentTime, chatType, chatRoomType, groupId, cName, cNumber, shouldSign, context);
        } else if (chatType.equals(ChatType.contact)) {
            sendChatMessage(userID, msg, uniqueID, currentTime, chatType, chatRoomType, groupId, cName, cNumber, shouldSign, context);
        } else if (chatType.equals(ChatType.sticker)) {
            sendChatMessage(userID, msg, uniqueID, currentTime, chatType, chatRoomType, groupId, cName, cNumber, shouldSign, context);

        }

//
//        if (jsonArray.length() > 0) {
//
//            int length = jsonArray.length() - 1;
//            final JSONObject jsonObject = jsonArray.optJSONObject(length);
//            String msgg = jsonObject.optString("msg");
//
//            if (msgg.contains("Yesterday")) {
//                autoReplyData = "Yesterday";
//            } else {
//                autoReplyData = "Tomorrow";
//            }
//        }
//
//        auto_complete_list = Const.dummyAutoReply(this, autoReplyData);
//        autoReplyAdapter.notifyDataSetChanged();


    }


    public void addNotifiHeader(String header, String currentTime, String time, String chatRoomType, String groupId, Context context) {
        DBHandler dbHandler = new DBHandler(context);
        dbHandler.InsertChatMsg(new ChatsMessagesModel(time, userIds, "0", "0", "0", header, groupId, currentTime, "0", "0", 0, 0, 0, String.valueOf(lat), String.valueOf(lng), "", "", chatRoomType, "false", "", "", 0, "", "", "", ""));
        dbHandler.close();


    }

    private void addMessgeToDb(String userID, String msg, String uniqueID, String currentTime, String sender,
                       ChatType chatType, String groupName, String create_by, String sent_by, String groupId, String chatRoomType, String cName, String cNumber, String shouldSign, Context context) {
        DBHandler dbHandler = new DBHandler(context);
        if (chatRoomType.equalsIgnoreCase("0")) {
            if (!dbHandler.DoesChatsUser(userIds)) {
                dbHandler.InsertChats(new ChatsModel(userIds, chatRoomType, sender, msg, Status.SENDING.toString(), currentTime, chatType.toString(), 0, groupName, create_by, sent_by, "", "0", "0", "", "", "", "", "", "", shouldSign, "1", "",""));
            } else {
                String isDeleted = dbHandler.getDeleteStatus(userIds);
                if (isDeleted.equalsIgnoreCase("1")) {
                    dbHandler.GroupDeleteUpdate("0", userIds);
                }
                dbHandler.UpdateLastMsg(userID, sender, msg, Status.SENDING.toString(), currentTime, chatType.toString(), 0);
            }
        } else {
            if (!dbHandler.DoesChatsUser(groupId)) {
                dbHandler.InsertChats(new ChatsModel(groupId, chatRoomType, sender, msg, Status.SENDING.toString(), currentTime, chatType.toString(), 0, groupName, create_by, sent_by, "", "0", "0", "", "", "", "", "", "", shouldSign, "1", "",""));
            } else {
                String isDeleted = dbHandler.getDeleteStatus(groupId);
                if (isDeleted.equalsIgnoreCase("1")) {
                    dbHandler.GroupDeleteUpdate("0", groupId);
                }
                dbHandler.UpdateLastMsg(groupId, sender, msg, Status.SENDING.toString(), currentTime, chatType.toString(), 0);
            }
        }

        if (!chatRoomType.equalsIgnoreCase("2")) {
            dbHandler.InsertChatMsg(new ChatsMessagesModel(uniqueID.trim(), userIds, msg, chatType.toString(), Status.SENDING.toString(), sender, groupId, currentTime, "0", "0", 0, 0, 0, String.valueOf(0), String.valueOf(lng), cName, cNumber, chatRoomType, "false", msg, "", (showPreview) ? 1 : 0, title, description, logo, shouldSign));
        } else {
            dbHandler.InsertChatMsg(new ChatsMessagesModel(uniqueID.trim(), userIds, msg, chatType.toString(), Status.SENDING.toString(), sender, groupId, currentTime, "0", "0", 0, 0, 0, String.valueOf(lat), String.valueOf(lng), cName, cNumber, chatRoomType, "false", msg, "", (showPreview) ? 1 : 0, title, description, logo, shouldSign));
//            new ChatActivity.addMessgeToDbMul(msg, chatType.toString(), Status.SENT.toString(), sender, "", currentTime, "0", "0", 0, 0, 0, String.valueOf(lat), String.valueOf(lng), cName, cNumber, "0", "false", msg).execute();
        }

        dbHandler.close();
    }

    // send the message
    public void sendChatMessage(String userID, String msg, String uniqueID, String currentTime, ChatType chatType, String chatRoomType, String groupId, String cName, String cNumber, String shouldSign, Context context) {


        ServiceClasss.Emitters emitters = new ServiceClasss.Emitters(context);
        AsyncTask<Void, Void, Void> obj = null;
        emitters.sendChat(userID, msg, uniqueID, currentTime, chatType, chatRoomType, groupId, cName, cNumber, shouldSign, "", "", "", false, obj, 0, 0);


    }
}