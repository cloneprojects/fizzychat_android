package com.app.fizzychat.Service;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.util.Log;

import com.app.fizzychat.baseUtils.SharedHelper;

import java.util.List;

import io.socket.client.Socket;

/**
 * Created by user on 25-07-2017.
 */

public class myBroadcastReceiver extends BroadcastReceiver {

    private Socket socket;

    @Override
    public void onReceive(Context context, Intent intent) {
        String action=intent.getAction();
        Log.d("myBroadCast", "action:"+action);

        Boolean isBacgkround = isAppIsInBackground(context);
        if (!isBacgkround) {

            ServiceClasss.Emitters emitters = new ServiceClasss.Emitters(context);
            emitters.emitonline();
        }
        if (SharedHelper.getKey(context, "firstTimeToApp").equalsIgnoreCase("")) {

//            SharedHelper.putKey(MainActivity.this, "firstTimeToApp", "false");
//            Log.e(TAG, "onLineCalledFromMain " + true);
//            ServiceClasss.Emitters emitters = new ServiceClasss.Emitters(MainActivity.this);
//            emitters.emitonline();

        } else {
            Log.e("Mybroad", "onReceive: " + "started");
            if (isOnline(context)) {
                if (!isMyServiceRunning(ServiceClasss.class, context)) {
                    context.startService(new Intent(context, ServiceClasss.class));
                }
            }

        }


    }


    private boolean isAppIsInBackground(Context context) {
        boolean isInBackground = true;
        try {
            ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
                List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
                for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                    if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                        for (String activeProcess : processInfo.pkgList) {
                            if (activeProcess.equals(context.getPackageName())) {
                                isInBackground = false;
                            }
                        }
                    }
                }
            } else {
                List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
                ComponentName componentInfo = taskInfo.get(0).topActivity;
                if (componentInfo.getPackageName().equals(context.getPackageName())) {
                    isInBackground = false;
                }
            }
        } catch (Exception e) {

        }
        return isInBackground;
    }
    public boolean isOnline(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return (netInfo != null && netInfo.isConnected());
    }

    private boolean isMyServiceRunning(Class<?> serviceClass, Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i("Service already", "running");
                return true;
            }
        }
        Log.i("Service not", "running");
        return false;
    }
}